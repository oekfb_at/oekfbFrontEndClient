import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Table, message, Tag, Button } from 'antd';
import ApiService from '../../../network/ApiService';

/**
 * RechnungenIndex component fetches and displays a list of Rechnungen.
 *
 * @component
 */
export default function RechnungenIndex() {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        /**
         * Fetches the rechnungen data from the API and sets it in the state.
         */
        const fetchData = async () => {
            const apiService = new ApiService();
            try {
                const data = await apiService.get('finanzen?per=1000');
                if (data && data.items) {
                    setDataSource(data.items);
                }
            } catch (error) {
                message.error('Failed to fetch rechnungen');
                console.error('Fetch rechnungen error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, []);

    const handleRechnungGleichen = async (id) => {
        const apiService = new ApiService();
        try {
            const response = await apiService.get(`finanzen/complete/${id}`);
            if (response) {
                message.success('Rechnung erfolgreich ausgeglichen');
                // Refresh the table data
                fetchData();
            } else {
                console.log('Fehler beim Ausgleichen der Rechnung');
            }
        } catch (error) {
            message.error('Fehler beim Ausgleichen der Rechnung');
            console.error('Rechnung Gleichen error:', error);
        }
    };

    const columns = [
        {
            title: 'Rechnungsnummer',
            dataIndex: 'number',
        },
        {
            title: 'Team',
            dataIndex: 'team_name',
        },
        {
            title: 'Betrag',
            dataIndex: 'summ',
            render: (summ) => `${summ} €`,
        },
        // {
        //     title: 'Status',
        //     dataIndex: 'status',
        //     render: (status) => (
        //         <Tag color={status === 'offen' ? 'orange' : 'green'}>{status}</Tag>
        //     ),
        // },
        {
            title: 'Kennzeichen',
            dataIndex: 'kennzeichen',
        },
        {
            title: 'Erstellungdatum',
            dataIndex: 'created',
            render: (created) => new Date(created).toLocaleDateString(),
        },
        {
            title: 'Zahlungsziel',
            dataIndex: 'due_date',
            render: (due_date) => due_date ? new Date(due_date).toLocaleDateString() : 'N/A',
        },
        // {
        //     title: 'Aktionen',
        //     dataIndex: 'actions',
        //     render: (_, record) => (
        //         <Button
        //             type="primary"
        //             onClick={() => handleRechnungGleichen(record.id)}
        //             disabled={record.status !== 'offen'}
        //         >
        //             Rechnung Gleichen
        //         </Button>
        //     ),
        // },
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Rechnungen Index</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Rechnungen Index" style={{ marginTop: 16 }}>
                <Table dataSource={dataSource} columns={columns} loading={loading} rowKey="id" />
            </Card>
        </div>
    );
}
