import React, { useState, useEffect } from 'react';
import { Modal, Input, Table, Button, Spin } from 'antd';
import ApiService from "../../../network/ApiService";

const { Search } = Input;

const TeamRechnungModal = ({ visible, onClose, onTeamSelect }) => { // Add onTeamSelect as a prop
    const [teams, setTeams] = useState([]);
    const [loadingTeams, setLoadingTeams] = useState(false);
    const [selectedTeam, setSelectedTeam] = useState(null);

    useEffect(() => {
        if (visible) {
            fetchTeams();
        }
    }, [visible]);

    const fetchTeams = async () => {
        setLoadingTeams(true);
        try {
            const response = await new ApiService().get('teams?per=20');
            setTeams(Array.isArray(response) ? response : response.items || []);
        } catch (error) {
            console.error('Failed to fetch teams:', error);
        } finally {
            setLoadingTeams(false);
        }
    };

    const handleSearch = async (value) => {
        setLoadingTeams(true);
        try {
            const endpoint = value ? `teams/search/${value}` : `teams?per=20`;
            const response = await new ApiService().get(endpoint);
            setTeams(Array.isArray(response) ? response : response.items || []);
        } catch (error) {
            console.error('Failed to fetch teams:', error);
        } finally {
            setLoadingTeams(false);
        }
    };

    const handleSelectTeam = () => {
        if (selectedTeam) {
            onTeamSelect(selectedTeam); // Pass the selected team back to the parent component
            onClose();
        }
    };

    const columns = [
        {
            title: 'Logo',
            dataIndex: 'logo',
            key: 'logo',
            render: (logo) => logo ? <img src={`${logo}`} alt="logo" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        { title: 'Mannschaft', dataIndex: 'team_name', key: 'team_name' },
        { title: 'SID', dataIndex: 'sid', key: 'sid' },
    ];

    return (
        <Modal
            title="Team Auswahl"
            visible={visible}
            onCancel={onClose}
            footer={[
                <Button key="back" onClick={onClose}>
                    Abbrechen
                </Button>,
                <Button key="submit" type="primary" onClick={handleSelectTeam} disabled={!selectedTeam}>
                    Team auswählen
                </Button>
            ]}
        >
            <p>Mannschaft mit sID oder Namen suchen:</p>
            <Search
                placeholder="Mannschaft mit sID oder Namen suchen"
                onSearch={handleSearch}
                enterButton
                style={{ marginBottom: 16 }}
            />
            {loadingTeams ? (
                <Spin size="large" />
            ) : (
                <Table
                    columns={columns}
                    dataSource={teams}
                    rowKey="id"
                    onRow={(record) => ({
                        onClick: () => setSelectedTeam(record),
                        style: { cursor: 'pointer', backgroundColor: selectedTeam?.id === record.id ? '#e6f7ff' : '' }
                    })}
                />
            )}
        </Modal>
    );
};

export default TeamRechnungModal;
