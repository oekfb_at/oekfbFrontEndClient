import React from "react";
import { useSelector } from "react-redux";
import { Row, Col, Card, Divider, Table } from "antd";
import logo from "../../../assets/images/logo/logo.svg";
import logoDark from "../../../assets/images/logo/logo-dark.svg";

export default function InvoiceCard({ team, amount, kennzeichen }) {
  const customise = useSelector((state) => state.customise);

  const rechnungsnummer = `${new Date().getFullYear()}${Math.floor(
      10000 + Math.random() * 90000
  )}`;
  const ausstellungsdatum = new Date().toLocaleDateString();
  const zuZahlenBis = new Date(Date.now() + 12096e5).toLocaleDateString(); // 2 weeks from today


  const columns = [
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
      render: (text) => <p>{text}</p>,
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
      render: (text) => (
          <h5 className="hp-text-align-right">
            {text}
          </h5>
      ),
      align: "right",
    },
  ];

  const data = [
    {
      key: "1",
      description: kennzeichen,
      price: `€ ${amount}`,
    },
  ];

  return (
      <Card id="invoice" className="hp-mb-32 hp-invoice-card">
        <Row justify="space-between">
          <Col xl={12} xs={24} sm={12}>
            {customise.theme === "light" ? (
                <img className="hp-logo hp-mb-16" src={logo} alt="logo" />
            ) : (
                <img className="hp-logo hp-mb-16" src={logoDark} alt="logo" />
            )}
          </Col>

          <Col>
            <p className="hp-p1-body hp-mb-16">
              RECHNUNGS NUMMER: {rechnungsnummer}
            </p>
          </Col>

          <Col>
            <p>Österreichischer Kleinfeld Fußball Bund</p>
            <p>1020 Wien, Pazmanitengasse 15/7</p>
            <p>office@oekfb.eu</p>
          </Col>
        </Row>

        <Divider />

        <Row justify="space-bewtween">
          <Col md={16} xs={24} className="hp-pb-16 hp-print-info">
            <p className="hp-text-color-black-100 hp-text-color-dark-0 hp-input-label">
              KUNDEN INFORMATION:
            </p>
            <p>Mannschaft: {team?.team_name}</p>

            <p
                className="hp-text-color-black-100 hp-text-color-dark-0 hp-input-label"
                style={{ marginTop: "20px" }}
            >
              AUFTRAG STATUS:
            </p>
            <p>Status: Pending</p>
          </Col>

          <Col
              md={8}
              xs={24}
              className="hp-text-sm-left hp-text-right hp-print-info"
          >
            <p>Ausstellungsdatum: {ausstellungsdatum}</p>
            <p>Zu Zahlen bis: {zuZahlenBis}</p>
          </Col>
        </Row>

        <Divider />

        <Table columns={columns} dataSource={data} bordered={false} pagination={false} />
        <Divider />
      </Card>
  );
}
