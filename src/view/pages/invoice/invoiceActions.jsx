import React, { useState } from "react";
import { Row, Col, Card, Button, Input, Drawer, Form, InputNumber } from "antd";
import { RiCloseFill } from "react-icons/ri";
import TeamRechnungModal from "./TeamRechnungModal";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import ApiService from "../../../network/ApiService";

export default function InvoiceActions({ onCreate }) {
  const [visible, setVisible] = useState(false);
  const [teamModalVisible, setTeamModalVisible] = useState(false);
  const [selectedTeam, setSelectedTeam] = useState(null);
  const [amount, setAmount] = useState(null);
  const [kennzeichen, setKennzeichen] = useState("");

  const konfig = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  const onSelectTeam = () => {
    setTeamModalVisible(true);
  };

  const invoiceData = {
    team: selectedTeam,
    amount,
    kennzeichen,
  };

  const handleErstellen = async () => {
    if (!invoiceData) {
      message.error("Please preview the invoice before creating it.");
      return;
    }

    const rechnungsnummer = `${new Date().getFullYear()}${Math.floor(10000 + Math.random() * 90000)}`;
    function toNegative(x) {
      return x - (x * 2);
    }

    const postData = {
      team: {
        id: invoiceData.team.id,
      },
      status: "offen",
      teamName: invoiceData.team.team_name,
      number: rechnungsnummer,
      summ: toNegative(invoiceData.amount),
      kennzeichen: invoiceData.kennzeichen,
    };

    try {
      console.log(invoiceData.team.team_name);
      // POST request using ApiService
      await new ApiService().post('/finanzen', postData);
      console.log('Invoice created successfully!');
      // Refresh the page
      window.location.reload();
    } catch (error) {
      console.error("Error creating invoice:", error);
    }
  };

  const handleTeamSelect = (team) => {
    console.log("Selected team:", team);
    setSelectedTeam(team);
    setTeamModalVisible(false);
  };

  const handleCreate = () => {
    console.log("Invoice data being created:", invoiceData);
    onCreate(invoiceData);
    onClose();
  };

  const handleDownload = () => {
    const input = document.getElementById("invoice");
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF();
      const imgProps = pdf.getImageProperties(imgData);
      const pdfWidth = pdf.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      pdf.addImage(imgData, "PNG", 0, 0, pdfWidth, pdfHeight);
      pdf.save("invoice.pdf");
    });
  };

  return (
      <Card className="hp-mb-32">
        <Button style={{ marginBottom: "20px"}} onClick={konfig} className="hp-mb-16" type="primary" block >
          Konfigurien
        </Button>

        <Button style={{ marginBottom: "20px"}} onClick={handleDownload} type="primary" block>
          Download
        </Button>

        <Button onClick={handleErstellen} type="ghost" block>
          Erstellen
        </Button>

        <Drawer
            className="hp-drawer-submit"
            title="Send Invoice"
            placement="right"
            closable={false}
            onClose={onClose}
            visible={visible}
            width={400}
            bodyStyle={{ paddingBottom: 80 }}
            closeIcon={<RiCloseFill className="remix-icon hp-text-color-black-80" size={24} />}
        >
          <Form layout="vertical" hideRequiredMark>
            <Row gutter={[16, 16]}>
              <Col span={24}>
                <Form.Item label="Mannschaft auswählen" rules={[{ required: true, message: "Bitte wählen Sie ein Team" }]}>
                  {selectedTeam ? (
                      <p>{selectedTeam.team_name}</p>
                  ) : (
                      <Button type="primary" onClick={onSelectTeam}>
                        Team auswählen
                      </Button>
                  )}
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                    name="amount"
                    label="Betrag"
                    rules={[{ required: true, message: "Bitte Betrag eingeben" }]}
                >
                  <InputNumber
                      placeholder="Betrag"
                      value={amount}
                      onChange={(value) => setAmount(value)}
                      formatter={(value) => `€ ${value}`}
                      parser={(value) => value.replace('€', '').trim()}
                      style={{ width: "100%" }}
                  />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Form.Item
                    name="kennzeichen"
                    label="Kennzeichen"
                    rules={[{ required: true, message: "Bitte Kennzeichen eingeben" }]}
                >
                  <Input
                      placeholder="Kennzeichen"
                      value={kennzeichen}
                      onChange={(e) => setKennzeichen(e.target.value)}
                  />
                </Form.Item>
              </Col>

              <Col span={24}>
                <Button
                    onClick={handleCreate}
                    type="primary"
                    className="hp-mr-8"
                >
                  Create
                </Button>

                <Button onClick={onClose} type="text">
                  Cancel
                </Button>
              </Col>
            </Row>
          </Form>
        </Drawer>

        <TeamRechnungModal
            visible={teamModalVisible}
            onClose={() => setTeamModalVisible(false)}
            onTeamSelect={handleTeamSelect}
        />
      </Card>
  );
}
