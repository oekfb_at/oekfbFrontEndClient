import React, { useState } from "react";
import { Row, Col } from "antd";
import BreadCrumbs from "../../../layout/components/content/breadcrumbs";
import InvoiceActions from "./invoiceActions";
import InvoiceCard from "./invoice";

export default function Invoice() {
    const [invoiceData, setInvoiceData] = useState({
        team: null,
        amount: null,
        kennzeichen: "",
    });

    function print() {
        window.print();
    }

    const handleCreate = (data) => {
        setInvoiceData(data);
    };

    return (
        <Row gutter={32}>
            <Col className="hp-mb-32" span={24}>
                <Row
                    gutter={[32, 32]}
                    justify="space-between"
                    className="hp-print-none"
                >
                    <BreadCrumbs breadCrumbParent="Rechnungen Index" breadCrumbActive="Invoice" />
                </Row>
            </Col>

            <Col xl={16} xs={24}>
                <InvoiceCard
                    team={invoiceData.team}
                    amount={invoiceData.amount}
                    kennzeichen={invoiceData.kennzeichen}
                />
            </Col>

            <Col xl={8} xs={24} className="hp-print-none">
                <InvoiceActions print={print} onCreate={handleCreate} />
            </Col>
        </Row>
    );
}
