import React from 'react';
import { Button } from 'antd';
import { RiFilePdfLine, RiImage2Line } from 'react-icons/ri';

const FileAttachment = ({ url }) => {
    const isImage = (fileUrl) => {
        const imageExtensions = ['.png', '.jpeg', '.jpg', '.gif'];
        return imageExtensions.some(ext => fileUrl.includes(ext));
    };

    const isPDF = (fileUrl) => fileUrl.includes('.pdf');

    const getFileName = (fileUrl) => {
        let fileName = fileUrl.split('/').pop();
        if (fileName.length > 30) {
            fileName = fileName.substring(0, 30) + '...';
        }
        return fileName;
    };

    return (
        <div style={{ display: 'flex', alignItems: 'center', marginTop: 2 }}>
            {isImage(url) ? (
                <RiImage2Line style={{ fontSize: '24px', marginRight: '5px' }} />
            ) : isPDF(url) ? (
                <RiFilePdfLine style={{ fontSize: '24px', marginRight: '5px' }} />
            ) : null}
            <Button
                type="text"
                href={url}
                target="_blank"
                rel="noopener noreferrer"
                style={{ margin: '0', padding: '0' }}
            >
                {getFileName(url)}
            </Button>
        </div>
    );
};

export default FileAttachment;
