import React, { useState, useEffect, useRef } from 'react';
import { useParams } from 'react-router-dom';
import { Card, Input, Button, List, Spin, Layout, Typography, Breadcrumb } from 'antd';
import ApiService from "../../../../../network/ApiService";
import './ChatApp.css';
import NewConversationModal from './NewConversationModal';
import FileAttachment from './FileAttachment';
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";

const { TextArea } = Input;
const { Sider, Content } = Layout;
const { Title } = Typography;
const { Item: BreadcrumbItem } = Breadcrumb;

const ChatView = () => {
    const { conversationId } = useParams();
    const [conversations, setConversations] = useState([]);
    const [selectedConversation, setSelectedConversation] = useState(null);
    const [loadingConversations, setLoadingConversations] = useState(true);
    const [loadingConversation, setLoadingConversation] = useState(false);
    const [newMessage, setNewMessage] = useState("");
    const [attachments, setAttachments] = useState([]);
    const [sending, setSending] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const messagesEndRef = useRef(null);

    useEffect(() => {
        fetchConversations();
    }, []);

    useEffect(() => {
        if (conversationId) {
            fetchConversation(conversationId);
        }
    }, [conversationId]);

    const fetchConversations = async () => {
        setLoadingConversations(true);
        try {
            const response = await new ApiService().get('chat/teams?per=20');
            setConversations(response || []);
        } catch (error) {
            console.error('Failed to fetch conversations:', error);
        } finally {
            setLoadingConversations(false);
        }
    };

    const fetchConversation = async (id) => {
        setLoadingConversation(true);
        try {
            const response = await new ApiService().get(`chat/${id}`);
            setSelectedConversation(response);
            scrollToBottom();
        } catch (error) {
            console.error('Failed to fetch conversation:', error);
        } finally {
            setLoadingConversation(false);
        }
    };

    const handleUploadSuccess = (fileUrl) => {
        setAttachments([...attachments, fileUrl]);
    };

    const sendMessage = async () => {
        if (!newMessage.trim() && attachments.length === 0) return;
        setSending(true);
        try {
            const message = {
                senderTeam: false,
                senderName: "OEKFB Administrator",
                text: newMessage,
                attachments: attachments
            };
            await new ApiService().post(`chat/${selectedConversation.id}/message`, message);
            setNewMessage("");
            setAttachments([]);
            fetchConversation(selectedConversation.id);
        } catch (error) {
            console.error('Failed to send message:', error);
        } finally {
            setSending(false);
        }
    };

    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
    };

    const handleConversationClick = (id) => {
        if (id !== selectedConversation?.id) {
            setSelectedConversation(null);
            fetchConversation(id);
        }
    };

    const handleModalClose = () => {
        setModalVisible(false);
        fetchConversations();
    };

    return (
        <div>
            <Breadcrumb style={{ margin: '16px 0' }}>
                <BreadcrumbItem>
                    <a href="/admin/">Administration</a>
                </BreadcrumbItem>
                <BreadcrumbItem>Admin Nachrichten</BreadcrumbItem>
            </Breadcrumb>

            <Layout style={{ height: 'calc(100vh - 32px)' }}>
                <Sider width="33%" style={{ background: '#fff', overflow: 'auto', borderRight: '1px solid #ddd' }}>
                    <Button type="primary" style={{ margin: '16px' }} onClick={() => setModalVisible(true)}>Neue Nachricht Schreiben</Button>
                    {loadingConversations ? (
                        <Spin size="large" />
                    ) : (
                        <List
                            dataSource={conversations.sort((a, b) => {
                                const aDate = a.messages?.[0]?.created ? new Date(a.messages[0].created) : new Date(0);
                                const bDate = b.messages?.[0]?.created ? new Date(b.messages[0].created) : new Date(0);
                                return bDate - aDate;
                            })}
                            renderItem={conversation => (
                                <List.Item
                                    key={conversation.id}
                                    onClick={() => handleConversationClick(conversation.id)}
                                    style={{ cursor: 'pointer', padding: '16px', borderBottom: '1px solid #ddd' }}
                                >
                                    <List.Item.Meta
                                        title={conversation.subject}
                                        description={`${conversation.team?.team_name || 'N/A'}`}
                                    />
                                </List.Item>
                            )}
                        />
                    )}
                </Sider>

                <Content style={{ display: 'flex', flexDirection: 'column', padding: '24px', overflow: 'auto' }}>
                    {loadingConversation ? (
                        <Spin size="large" />
                    ) : selectedConversation ? (
                        <>
                            <Card title={`Betreff: ${selectedConversation.subject}`} style={{ marginBottom: 16, flexGrow: 1 }}>
                                <p>{selectedConversation.team?.team_name}</p>
                                <List
                                    dataSource={selectedConversation.messages || []}
                                    renderItem={message => (
                                        <List.Item key={message.id} className={message.sender_team ? 'message-right' : 'message-left'}>
                                            <div className="message-header">
                                                <span>{message.sender_team ? "Team" : "OEKFB Administrator"}</span>
                                                <span>{new Date(message.created).toLocaleString()}</span>
                                            </div>
                                            <div className="message-content">{message.text}</div>
                                            {message.attachments && message.attachments.length > 0 && (
                                                <div className="attachment-list">
                                                    {message.attachments.map(url => (
                                                        <FileAttachment key={url} url={url} />
                                                    ))}
                                                </div>
                                            )}
                                        </List.Item>
                                    )}
                                    style={{ maxHeight: '60vh', overflow: 'auto' }}
                                />
                                <div ref={messagesEndRef} />
                            </Card>
                            <div style={{ marginTop: 'auto' }}>
                                {selectedConversation.id && (
                                    <FirebaseImageUpload
                                        onUploadSuccess={handleUploadSuccess}
                                        path={`conversations/${selectedConversation.id}/files`}
                                        filename={Date.now().toString()}
                                    />
                                )}

                                <TextArea
                                    rows={4}
                                    value={newMessage}
                                    onChange={(e) => setNewMessage(e.target.value)}
                                    placeholder="Type your message here..."
                                />
                                <Button
                                    type="primary"
                                    onClick={sendMessage}
                                    loading={sending}
                                    style={{ marginTop: 16 }}
                                >
                                    Send
                                </Button>
                            </div>
                        </>
                    ) : (
                        <p>Select a conversation to view messages</p>
                    )}
                </Content>

                <NewConversationModal
                    visible={modalVisible}
                    onClose={handleModalClose}
                />
            </Layout>
        </div>
    );
};

export default ChatView;
