import React, { useState, useEffect } from 'react';
import { Modal, Input, Table, Button, Spin } from 'antd';
import ApiService from "../../../../../network/ApiService";

const { Search } = Input;

const NewConversationModal = ({ visible, onClose }) => {
    const [teams, setTeams] = useState([]);
    const [loadingTeams, setLoadingTeams] = useState(false);
    const [selectedTeam, setSelectedTeam] = useState(null);
    const [subject, setSubject] = useState('');

    useEffect(() => {
        if (visible) {
            fetchTeams();
        }
    }, [visible]);

    const fetchTeams = async () => {
        setLoadingTeams(true);
        try {
            const response = await new ApiService().get('teams?per=20');
            setTeams(Array.isArray(response) ? response : response.items || []);
        } catch (error) {
            console.error('Failed to fetch teams:', error);
        } finally {
            setLoadingTeams(false);
        }
    };

    const handleSearch = async (value) => {
        setLoadingTeams(true);
        try {
            const endpoint = value ? `teams/search/${value}` : `teams?per=20`;
            const response = await new ApiService().get(endpoint);
            setTeams(Array.isArray(response) ? response : response.items || []);
        } catch (error) {
            console.error('Failed to fetch teams:', error);
        } finally {
            setLoadingTeams(false);
        }
    };

    const startNewConversation = async () => {
        if (!selectedTeam || !subject.trim()) return;
        try {
            const conversation = {
                team: { id: selectedTeam.id },
                subject: subject.trim(),
                messages: []
            };
            await new ApiService().post('chat/', conversation);
            onClose();
        } catch (error) {
            console.error('Failed to start new conversation:', error);
        }
    };

    const columns = [
        {
            title: 'Logo',
            dataIndex: 'logo',
            key: 'logo',
            render: (logo) => logo ? <img src={`${logo}`} alt="logo" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        { title: 'Mannschaft', dataIndex: 'team_name', key: 'team_name' },
        { title: 'SID', dataIndex: 'sid', key: 'sid' },
    ];

    return (
        <Modal
            title="Neue Nachricht"
            visible={visible}
            onCancel={onClose}
            footer={[
                <Button key="back" onClick={onClose}>
                    Abbrechen
                </Button>,
                <Button key="submit" type="primary" onClick={startNewConversation} disabled={!selectedTeam || !subject.trim()}>
                    Chat starten
                </Button>
            ]}
        >
            <p>Was ist der Betreff dieser Nachricht:</p>
            <Input
                placeholder="Betreff"
                value={subject}
                onChange={(e) => setSubject(e.target.value)}
                style={{ marginBottom: 16 }}
            />
            <p>Mannschaft mit sID oder Namen suchen:</p>
            <Search
                placeholder="Mannschaft mit sID oder Namen suchen"
                onSearch={handleSearch}
                enterButton
                style={{ marginBottom: 16 }}
            />
            {loadingTeams ? (
                <Spin size="large" />
            ) : (
                <Table
                    columns={columns}
                    dataSource={teams}
                    rowKey="id"
                    onRow={(record) => ({
                        onClick: () => setSelectedTeam(record),
                        style: { cursor: 'pointer', backgroundColor: selectedTeam?.id === record.id ? '#e6f7ff' : '' }
                    })}
                />
            )}
        </Modal>
    );
};

export default NewConversationModal;
