import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Table, message, Button, Tag } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';

/**
 * RegistrationIndex component fetches and displays a list of registrations.
 * Users can navigate to the RegistrationDetail component by clicking on the Details button.
 *
 * @component
 */
export default function RegistrationIndex() {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    useEffect(() => {
        /**
         * Fetches the registration data from the API and sets it in the state.
         */
        const fetchData = async () => {
            const apiService = new ApiService();
            try {
                const data = await apiService.get('registrations?per=1000');
                if (data && data.items) {
                    setDataSource(data.items);
                }
            } catch (error) {
                message.error('Failed to fetch registrations');
                console.error('Fetch registrations error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, []);

    /**
     * Sets the registration detail in session storage.
     *
     * @param {Object} value - The registration detail to be stored.
     */
    const setRegistrationDetail = (value) => {
        console.log("Setting registration detail in session storage:", value);
        sessionStorage.setItem("registrationDetail", JSON.stringify(value));
    };

    const columns = [
        {
            title: 'Primary Name',
            dataIndex: 'primary',
            render: (primary) => `${primary.first} ${primary.last}`,
        },
        {
            title: 'Primary Email',
            dataIndex: ['primary', 'email'],
        },
        {
            title: 'Primary Phone',
            dataIndex: ['primary', 'phone'],
        },
        {
            title: 'Bundesland',
            dataIndex: 'bundesland',
        },
        {
            title: 'Team Name',
            dataIndex: 'team_name',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            render: (status) => (
                <Tag color={status === 'draft' ? 'processing' : 'success'}>{status}</Tag>
            ),
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Button
                    type="text"
                    onClick={() => {
                        setRegistrationDetail(record);
                        history.push(`/admin/registration_detail/${record.id}`);
                    }}
                >
                    Details
                </Button>
            ),
        },
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Registrierung Index</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Registration Index" style={{ marginTop: 16 }}>
                <Table dataSource={dataSource} columns={columns} loading={loading} />
            </Card>
        </div>
    );
}
