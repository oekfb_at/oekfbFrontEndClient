import React, { useState, useEffect } from 'react';
import { Card, Descriptions, Input, Button, Select, message, Breadcrumb } from 'antd';
import { useHistory } from 'react-router-dom'; // Import useHistory from react-router-dom
import ApiService from "../../../../../network/ApiService";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";
import pdfIcon from "../../../../../assets/icons_dev/pdficon.png";

const { Option } = Select;

/**
 * Component for displaying and managing registration details.
 * @returns JSX element
 */
const RegistrationDetail = () => {
    const [registrationData, setRegistrationData] = useState(null);
    const [paymentAmount, setPaymentAmount] = useState('');
    const [leagues, setLeagues] = useState([]);
    const [selectedLeague, setSelectedLeague] = useState(null);
    const api = new ApiService();
    const history = useHistory(); // Access history object for navigation

    useEffect(() => {
        fetchRegistrationData();
        fetchLeagues();
    }, []);

    /**
     * Fetches registration data from the server and updates state.
     */
    const fetchRegistrationData = async () => {
        try {
            const storedRegistration = JSON.parse(sessionStorage.getItem("registrationDetail"));
            if (storedRegistration && storedRegistration.id) {
                const response = await api.get(`registrations/${storedRegistration.id}/`);
                setRegistrationData(response);
                setSelectedLeague(response.assigned_league || null);
            }
        } catch (error) {
            console.error('Failed to fetch registration data:', error);
            message.error('Failed to fetch registration data');
        }
    };

    /**
     * Fetches the list of leagues from the server and updates state.
     */
    const fetchLeagues = async () => {
        try {
            const response = await api.get('leagues?per=50');
            setLeagues(response.items || []);
        } catch (error) {
            console.error('Failed to fetch leagues:', error);
            message.error('Failed to fetch leagues');
        }
    };

    /**
     * Handles the update of payment amount for the registration.
     */
    const handleUpdatePayment = async () => {
        try {
            if (!registrationData) {
                throw new Error('Registration data not available');
            }

            await api.post(`registrations/updatePayment/${registrationData.id}`, {
                paidAmount: parseFloat(paymentAmount),
            });

            message.success('Payment updated successfully');
            fetchRegistrationData();
        } catch (error) {
            console.error('Failed to update payment:', error);
            message.error('Failed to update payment');
        }
    };

    /**
     * Handles the assignment of a league to the registration.
     */
    const handleAssignLeague = async () => {
        try {
            if (!registrationData || !selectedLeague) {
                throw new Error('Registration data or selected league not available');
            }

            await api.post(`registrations/assign/${registrationData.id}/league/${selectedLeague}`);

            message.success('League assigned successfully');
            fetchRegistrationData();
        } catch (error) {
            console.error('Failed to assign league:', error);
            message.error('Failed to assign league');
        }
    };

    /**
     * Handles the confirmation of registration after checking necessary conditions.
     */
    const handleConfirmRegistration = async () => {
        try {
            if (
                !registrationData ||
                registrationData.paid_amount === null ||
                registrationData.assigned_league === null ||
                registrationData.paid_amount < 0
            ) {
                throw new Error('Cannot confirm registration: Paid amount is missing, negative, or assigned league is missing');
            }

            await api.post(`registrations/confirm/${registrationData.id}`, registrationData);

            message.success('Registration confirmed successfully');
            fetchRegistrationData();
        } catch (error) {
            console.error('Failed to confirm registration:', error);
            message.error('Failed to confirm registration');
        }
    };

    /**
     * Handles the rejection of registration.
     */
    const handleRejectRegistration = async () => {
        try {
            if (!registrationData) {
                throw new Error('Registration data not available');
            }

            await api.post(`registrations/reject/${registrationData.id}`);

            message.success('Registration rejected successfully');
            fetchRegistrationData();
        } catch (error) {
            console.error('Failed to reject registration:', error);
            message.error('Failed to reject registration');
        }
    };

    /**
     * Handles the deletion of the registration.
     */
    const handleDeleteRegistration = async () => {
        try {
            if (!registrationData || !registrationData.id) {
                throw new Error('Registration ID not available');
            }

            await api.delete(`registrations/${registrationData.id}`);

            message.success('Registration deleted successfully');
            history.push('/admin/registrations'); // Navigate back to registrations list
        } catch (error) {
            console.error('Failed to delete registration:', error);
            message.error('Failed to delete registration');
        }
    };

    /**
     * Handles the upload of a file and updates the registration data accordingly.
     * @param {string} url - The URL of the uploaded file.
     * @param {string} field - The field in registrationData to update.
     */
    const handleUpload = async (url, field) => {
        try {
            const updatedData = {
                ...registrationData,
                primary: {
                    ...registrationData.primary,
                    identification: field === 'primary.identification' ? url : registrationData.primary.identification,
                },
                secondary: {
                    ...registrationData.secondary,
                    identification: field === 'secondary.identification' ? url : registrationData.secondary.identification,
                },
                admin_signed_contract: field === 'admin_signed_contract' ? url : null,
                customer_signed_contract: field === 'customer_signed_contract' ? url : null,
                team_logo: field === 'team_logo' ? url : null,
                // Ensure fields match the required structure
                user: null,
                is_welcome_email_sent: registrationData.is_welcome_email_sent !== null ? registrationData.is_welcome_email_sent : null,
                is_login_data_sent: registrationData.is_login_data_sent !== null ? registrationData.is_login_data_sent : null,
                refereer_link: registrationData.refereer_link || "",
                team_name: registrationData.team_name || "",
                verein: registrationData.verein || "",
                team: registrationData.team || null,
                status: registrationData.status || "draft",
                initial_password: registrationData.initial_password,
                paid_amount: registrationData.paid_amount !== null ? registrationData.paid_amount : null,
                bundesland: registrationData.bundesland || "",
                assigned_league: registrationData.assigned_league || null,
                date_created: registrationData.date_created || null,
            };

            await api.patch(`registrations/${registrationData.id}`, updatedData);
            message.success('File uploaded and registration updated successfully');
            fetchRegistrationData();
        } catch (error) {
            console.error(`Failed to update registration with uploaded file: ${field}`, error);
            message.error(`Failed to update registration with uploaded file: ${field}`);
        }
    };

    if (!registrationData) {
        return <p>Loading...</p>;
    }

    // Format number as Euros
    const formatAsEuros = (amount) => {
        return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(amount);
    };

    return (
        <div style={{ display: 'flex', flexDirection: 'column', gap: '16px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/admin/registrations">Registrations</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Registration Detail</Breadcrumb.Item>
                </Breadcrumb>
                <Button type="danger" onClick={handleDeleteRegistration}>DELETE</Button>
            </div>

            <Card title="Registration Information">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Team Name">{registrationData.team_name}</Descriptions.Item>
                    <Descriptions.Item label="Registrierungs ID">{registrationData.id}</Descriptions.Item>
                    <Descriptions.Item label="Status">{registrationData.status}</Descriptions.Item>
                    <Descriptions.Item label="Bundesland">{registrationData.bundesland}</Descriptions.Item>
                    <Descriptions.Item label="Verein">{registrationData.verein}</Descriptions.Item>
                    <Descriptions.Item label="Initial Password">{registrationData.initial_password}</Descriptions.Item>
                    <Descriptions.Item label="Referee Link">{registrationData.refereer_link}</Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Mannschaft Details">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Mannschaft Wappen/Logo">
                        <FirebaseImageUpload
                            existingUrl={registrationData.team_logo}
                            path={`registrations/${registrationData.initial_password}`}
                            filename={`${registrationData.initial_password}_logo`}
                            onUploadSuccess={(url) => handleUpload(url, 'team_logo')}
                        />
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Admin and Customer Contracts">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Admin Signed Contract">
                        <FirebaseImageUpload
                            existingUrl={registrationData.admin_signed_contract}
                            path={`registrations/${registrationData.initial_password}`}
                            filename={`${registrationData.initial_password}_admin_vertrag_unterschrieben`}
                            onUploadSuccess={(url) => handleUpload(url, 'admin_signed_contract')}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Customer Signed Contract">
                        <FirebaseImageUpload
                            existingUrl={registrationData.customer_signed_contract}
                            path={`registrations/${registrationData.initial_password}`}
                            filename={`${registrationData.initial_password}_kunden_vertrag_unterschrieben`}
                            onUploadSuccess={(url) => handleUpload(url, 'customer_signed_contract')}
                        />
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Primary Contact">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="First Name">{registrationData.primary?.first}</Descriptions.Item>
                    <Descriptions.Item label="Last Name">{registrationData.primary?.last}</Descriptions.Item>
                    <Descriptions.Item label="Email">{registrationData.primary?.email}</Descriptions.Item>
                    <Descriptions.Item label="Phone">{registrationData.primary?.phone}</Descriptions.Item>
                    <Descriptions.Item label="Identification">
                        <FirebaseImageUpload
                            existingUrl={registrationData.primary?.identification}
                            path={`registrations/${registrationData.initial_password}`}
                            filename={`${registrationData.initial_password}_primary_identification`}
                            onUploadSuccess={(url) => handleUpload(url, 'primary.identification')}
                        />
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Secondary Contact">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="First Name">{registrationData.secondary?.first}</Descriptions.Item>
                    <Descriptions.Item label="Last Name">{registrationData.secondary?.last}</Descriptions.Item>
                    <Descriptions.Item label="Email">{registrationData.secondary?.email}</Descriptions.Item>
                    <Descriptions.Item label="Phone">{registrationData.secondary?.phone}</Descriptions.Item>
                    <Descriptions.Item label="Identification">
                        <FirebaseImageUpload
                            existingUrl={registrationData.secondary?.identification}
                            path={`vertraege/${registrationData.initial_password}`}
                            filename={`${registrationData.initial_password}_secondary_identification`}
                            onUploadSuccess={(url) => handleUpload(url, 'secondary.identification')}
                        />
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Liga Zuweisung">
                <div>
                    <Select
                        style={{ width: '200px', marginBottom: '8px' }}
                        placeholder="Select a league"
                        value={selectedLeague}
                        onChange={(value) => setSelectedLeague(value)}
                    >
                        {leagues.map(league => (
                            <Option key={league.id} value={league.id}>{league.name}</Option>
                        ))}
                    </Select>
                    <Button type="primary" onClick={handleAssignLeague}>Assign League</Button>
                </div>
            </Card>

            <Card title="Zahlungsanforderung">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Offener Betrag (Teilbetrag inkl. €300 Kaution) ">
                        {registrationData.paid_amount !== null ? formatAsEuros(registrationData.paid_amount) : 'Liga muss zugewiesen sein'}
                    </Descriptions.Item>
                </Descriptions>

                <div style={{ margin: '16px 10xp' }}>
                    <p>
                        Um eine Überweisung zu bestätigen, geben Sie hier den Betrag ein, der eingegangen ist, und dieser wird der Registrierung gutgeschrieben. Wenn der Betrag höher ist als die Rechnung, wird er der Mannschaft gutgeschrieben. Um eine Summe abzuziehen, tragen Sie einfach die negative Summe ein (-100).
                    </p>
                    <Input
                        type="number"
                        placeholder="Betrag eingeben"
                        value={paymentAmount}
                        onChange={(e) => setPaymentAmount(e.target.value)}
                        style={{ marginRight: '8px', width: '200px' }}
                    />
                    <Button
                        type="primary"
                        disabled={!selectedLeague}
                        onClick={handleUpdatePayment}
                    >
                        Bezahlten Betrag Bestätigen
                    </Button>
                </div>
            </Card>

            <div style={{ marginTop: '16px' }}>
                <Button
                    type="primary"
                    onClick={handleConfirmRegistration}
                    disabled={registrationData.paid_amount < 0 || !registrationData.assigned_league || !registrationData.primary.identification}
                >
                    Confirm Registration
                </Button>
                <Button style={{ marginLeft: '8px' }} onClick={handleRejectRegistration}>Reject Registration</Button>
            </div>
        </div>
    );
};

export default RegistrationDetail;
