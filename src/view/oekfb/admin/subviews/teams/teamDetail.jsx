import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Breadcrumb, Card, Descriptions, Spin, Input, InputNumber, Button, Table, Modal, Select, message } from 'antd';
import ApiService from "../../../../../network/ApiService";
import AddTeamPlayerModal from "./AddTeamPlayerModal";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";

const { Option } = Select;

export default function TeamDetail() {
    const { id } = useParams(); // Get the team ID from the URL parameters
    const history = useHistory(); // Get the history object for navigation

    // State variables
    const [team, setTeam] = useState({
        points: 0,
        league_code: "",
        captain: "",
        sid: "",
        coverimg: "",
        membership_since: "",
        usremail: "",
        usrtel: "",
        user: { id: "" },
        trikot: { home: "", away: "" },
        average_age: "",
        league: { id: "" },
        teamName: "",
        coach: { name: "" },
        id: "",
        balance: 0,
        usrpass: "",
        foundation_year: "",
        refer_code: "",
        logo: "",
        referCode: "",
        kaution: 0
    });

    const [loading, setLoading] = useState(true); // Loading state for fetching data
    const [players, setPlayers] = useState([]); // List of players in the team
    const [isAddTeamModalVisible, setAddTeamModalVisible] = useState(false); // Visibility state for adding team modal
    const [originalTeam, setOriginalTeam] = useState(null); // Original team data for change detection
    const [isTopUpModalVisible, setTopUpModalVisible] = useState(false); // Visibility state for top-up modal
    const [topUpAmount, setTopUpAmount] = useState(0); // Amount for balance top-up
    const [leagues, setLeagues] = useState([]); // List of leagues for the league dropdown
    const [rechnungen, setRechnungen] = useState([]); // List of rechnungen (invoices)

    useEffect(() => {
        refresh();
        fetchLeagues();
    }, [id]);

    useEffect(() => {
        if (team.id) {
            fetchRechnungen(); // Fetch the rechnungen data only when team.id is available
        }
    }, [team.id]);

    // Function to refresh the team data
    const refresh = async () => {
        setLoading(true);
        try {
            const storedTeam = JSON.parse(sessionStorage.getItem("teamDetail")); // Get team details from session storage

            // If team details are stored in session, use them; otherwise, fetch from API
            if (storedTeam) {
                const response = await new ApiService().get(`teams/${storedTeam.id}`);
                const parsedTeam = parseTeamData(response);
                setTeam(parsedTeam);
                setOriginalTeam(parsedTeam);

                const playerResponse = await new ApiService().get(`teams/${response.id}/players`);
                setPlayers(playerResponse.players);
            } else {
                const response = await new ApiService().get(`teams/${id}`);
                const parsedTeam = parseTeamData(response);
                setTeam(parsedTeam);
                setOriginalTeam(parsedTeam);

                const playerResponse = await new ApiService().get(`teams/${response.id}/players`);
                setPlayers(playerResponse.players);
            }
        } catch (error) {
            console.error('Failed to fetch team details:', error);
        } finally {
            setLoading(false);
        }
    };

    // Function to fetch available leagues
    const fetchLeagues = async () => {
        try {
            const response = await new ApiService().get('leagues?per=50');
            setLeagues(response.items);
        } catch (error) {
            console.error('Failed to fetch leagues:', error);
        }
    };

    // Function to fetch rechnungen data
    const fetchRechnungen = async () => {
        if (!team.id) return;
        setLoading(true);
        try {
            const response = await new ApiService().get(`teams/${team.id}/rechungen`);
            setRechnungen(response.rechnungen || []);
        } catch (error) {
            message.error(`Failed to fetch rechnungen for team with ID: ${team.id}`);
            console.error('Fetch rechnungen error:', error);
        } finally {
            setLoading(false);
        }
    };

    // Function to parse and format team data
    const parseTeamData = (data) => {
        return {
            points: data.points || 0,
            league_code: data.league_code || "",
            captain: data.captain || "",
            sid: data.sid || "",
            coverimg: data.coverimg || "",
            membership_since: data.membership_since || "",
            usremail: data.usremail || "",
            usrtel: data.usrtel || "",
            user: data.user || { id: "" },
            trikot: data.trikot || { home: "", away: "" },
            average_age: data.average_age || "",
            league: data.league || { id: "" },
            teamName: data.team_name || "",
            coach: data.coach || { name: "" },
            id: data.id || "",
            balance: data.balance || 0,
            kaution: data.kaution || 0,
            usrpass: data.usrpass || "",
            foundation_year: data.foundation_year || "",
            refer_code: data.refer_code || "",
            logo: data.logo || "",
            referCode: data.referCode || ""
        };
    };

    // Function to generate a random refer code
    const generateReferCode = () => {
        return Math.random().toString(36).substring(2, 10).toUpperCase();
    };

    // Handle input changes for updating team details
// Handle input changes for updating team details
    const handleInputChange = (field, value) => {
        if (field.includes('.')) {
            const [parent, child] = field.split('.');
            setTeam((prevTeam) => ({
                ...prevTeam,
                [parent]: {
                    ...prevTeam[parent],
                    [child]: value,
                },
            }));
        } else {
            setTeam((prevTeam) => ({ ...prevTeam, [field]: value }));
        }
    };

    // Handle league selection changes
    const handleLeagueChange = async (value) => {
        setTeam({ ...team, league: { id: value } });
        try {
            await new ApiService().post(`teams/${team.id}/league/${value}`);
            await refresh(); // Refresh the data after updating
            message.success('League updated successfully');
        } catch (error) {
            message.error('Failed to update league');
            console.error('Update league error:', error);
        }
    };

    // Check if the team details have changed
    const hasTeamChanged = () => {
        return JSON.stringify(team) !== JSON.stringify(originalTeam);
    };

    // Show or hide the "Add Team Player" modal
    const showAddTeamModal = () => {
        setAddTeamModalVisible(true);
    };
    const handleAddTeamModalCancel = () => {
        setAddTeamModalVisible(false);
    };
    const handleAddTeamModalCreate = async () => {
        setAddTeamModalVisible(false);
        await refresh();
    };

    // Handle team update
// Handle team update
    const handleUpdateTeam = async () => {
        if (hasTeamChanged()) {
            const updatedTeam = { ...team };
            const apiService = new ApiService();

            try {
                await apiService.patch(`teams/${team.id}`, updatedTeam); // Send updated team with new trikot URLs
                sessionStorage.setItem("teamDetail", JSON.stringify(updatedTeam));
                message.success('Team updated successfully');
                await refresh();
            } catch (error) {
                message.error('Failed to update team');
                console.error('Update team error:', error);
            }
        } else {
            message.info('No changes to update');
        }
    };

    // Set player details in session storage for navigation to player details
    const setPlayerDetail = (value) => {
        sessionStorage.setItem("playerDetail", JSON.stringify(value));
    };

    // Show or hide the "Top Up" modal
    const showTopUpModal = () => {
        setTopUpModalVisible(true);
    };

    const handleTopUpModalCancel = () => {
        setTopUpModalVisible(false);
        setTopUpAmount(0);
    };

    // Handle balance top-up
    const handleTopUp = async () => {
        try {
            await new ApiService().get(`teams/${team.id}/topup/${topUpAmount}`);
            setTeam({ ...team, balance: (team.balance || 0) + topUpAmount });
            setTopUpAmount(0);
            setTopUpModalVisible(false);
            message.success('Balance updated successfully');
            await refresh();
        } catch (error) {
            message.error('Failed to update balance');
            console.error('Top up balance error:', error);
        }
    };

    // Handle invoice completion
    const handleRechnungGleichen = async (rechnungId) => {
        try {
            await new ApiService().get(`finanzen/complete/${rechnungId}`);
            message.success('Rechnung erfolgreich ausgeglichen');
            fetchRechnungen(); // Refresh the rechnungen data
        } catch (error) {
            message.error('Fehler beim Ausgleichen der Rechnung');
            console.error('Rechnung Gleichen error:', error);
        }
    };

    // Table columns for players
    const playerColumns = [
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (image) => image ? <img src={`${image}`} alt="player" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        { title: 'Name', dataIndex: 'name', key: 'name' },
        { title: 'SID', dataIndex: 'sid', key: 'sid' },
        { title: 'ID', dataIndex: 'id', key: 'id' },
        { title: 'Spielberechtigung', dataIndex: 'eligibility', key: 'eligibility' },
        { title: 'Nationality', dataIndex: 'nationality', key: 'nationality' },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button
                    type="text"
                    className="hp-text-color-primary-1 hp-hover-bg-primary-4"
                    onClick={() => {
                        setPlayerDetail(record);
                        history.push(`/admin/players_detail/${record.id}`);
                    }}
                >
                    Player Details
                </Button>
            ),
        }
    ];

    // Rechnungen table columns
    const rechnungenColumns = [
        { title: 'Rechnungsnummer', dataIndex: 'number' },
        { title: 'Betrag', dataIndex: 'summ', render: (summ) => `${summ} €` },
        { title: 'Kennzeichen', dataIndex: 'kennzeichen' },
        { title: 'Erstellungsdatum', dataIndex: 'created', render: (created) => new Date(created).toLocaleDateString() },
        { title: 'Zahlungsziel', dataIndex: 'due_date', render: (due_date) => due_date ? new Date(due_date).toLocaleDateString() : 'N/A' },
        // {
        //     title: 'Actions',
        //     key: 'actions',
        //     render: (_, record) => (
        //         <Button type="link" onClick={() => handleRechnungGleichen(record.id)}>
        //             Rechnung Ausgleichen
        //         </Button>
        //     ),
        // }
    ];

    // Render a loading spinner if data is still loading
    if (loading) {
        return <Spin size="large" />;
    }

    // Render a message if no team ID is found
    if (!team.id) {
        return <p>Team not found</p>;
    }

    // Render the team detail view
    return (
        <div style={{padding: 24}}>
            <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/admin/team">Mannschaften</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Team Detail</Breadcrumb.Item>
                </Breadcrumb>
                <Button
                    type="primary"
                    onClick={handleUpdateTeam}
                    disabled={!hasTeamChanged()}
                >
                    Update
                </Button>
            </div>

            {/* Team Cover Image */}
            <img src={team.coverimg} alt="cover"
                 style={{width: "50%", height: 'auto', marginTop: 16, objectFit: 'cover', marginBottom: "20px"}}/>
            <FirebaseImageUpload
                onUploadSuccess={(url) => handleInputChange('coverimg', url)} // Pass the correct URL
                path={`teams/${team.id}`}
                filename="coverimg"
                buttonText={"Mannschaftsfoto Hochladen"}
            />

            {/* Team Information Card */}
            <Card title="Team Information" style={{marginTop: 16}}>
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Team Name">
                        <Input
                            value={team.teamName}
                            onChange={(e) => handleInputChange('teamName', e.target.value)}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Logo">
                        {team.logo ? (
                            <div>
                                <img src={team.logo} alt="logo"
                                     style={{maxWidth: '100px', maxHeight: '100px', marginBottom: "15px"}}/>
                                <FirebaseImageUpload
                                    onUploadSuccess={(url) => handleInputChange('logo', url)} // Pass the correct URL
                                    path={`teams/${team.id}`}
                                    filename="logo"
                                    buttonText={"Wappen Aktualisieren"}
                                />

                            </div>
                        ) : (
                            <div>
                                <span>No logo available</span>
                                <FirebaseImageUpload
                                    onUploadSuccess={(url) => handleInputChange('logo', url)} // Pass the correct URL
                                    path={`teams/${team.id}`}
                                    filename="logo"
                                    buttonText={"Wappen Aktualisieren"}
                                />

                            </div>

                        )
                        }
                    </Descriptions.Item>
                    <Descriptions.Item label="Trikot">
        <div style={{display: 'flex', alignItems: 'center'}}>
            <div style={{marginRight: '30px'}}>
                <img src={`${team.trikot?.home}`} alt="trikot"
                     style={{height: '250px', marginBottom: "20px"}}/>
                <h5>HEIM TRIKO</h5>

                <FirebaseImageUpload
                    onUploadSuccess={(url) => handleInputChange('trikot.home', url)} // Pass the correct URL
                                    path={`teams/${team.id}`}
                                    filename="home_trikot"
                                    buttonText={"Heim Trikot Hochladen"}
                                />

                            </div>
                            <div>
                                <img src={`${team.trikot?.away}`} alt="trikot" style={{ height: '250px', marginBottom: "20px" }} />
                                <h5>AWAY TRIKO</h5>
                                <FirebaseImageUpload
                                    onUploadSuccess={(url) => handleInputChange('trikot.away', url)} // Pass the correct URL
                                    path={`teams/${team.id}`}
                                    filename="away_trikot"
                                    buttonText={"AWAY Trikot Hochladen"}
                                />
                            </div>
                        </div>
                    </Descriptions.Item>
                    <Descriptions.Item label="Team UUID">{team.id}</Descriptions.Item>
                    <Descriptions.Item label="Team Telefonnummer">
                        <Input
                            value={team.usrtel}
                            onChange={(e) => handleInputChange('usrtel', e.target.value)}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Captain">
                        {team.captain ? (
                            <img src={`${team.captain}`} alt="Captain" style={{ maxWidth: '100px', maxHeight: '100px' }} />

                        ) : (
                            <span>No captain image available</span>
                        )}
                    </Descriptions.Item>

                    <Descriptions.Item label="Offener Kaution Betrag">€ {team.kaution}</Descriptions.Item>
                    <Descriptions.Item label="Trainer Name">
                        <Input
                            value={team.coach?.name || ''}
                            onChange={(e) => setTeam(prev => ({
                                ...prev,
                                coach: { ...prev.coach, name: e.target.value }
                            }))}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Trainer Email Adresse">
                        <Input
                            value={team.coach?.email || ''}
                            onChange={(e) => setTeam(prev => ({
                                ...prev,
                                coach: { ...prev.coach, email: e.target.value }
                            }))}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Trainer Foto">
                        {team.coach?.image? (
                            <div>
                                <img src={`${team.coach.image}`} alt="Trainer" style={{ maxWidth: '100px', maxHeight: '100px' }} />
                                <FirebaseImageUpload
                                    onUploadSuccess={(url) => handleInputChange('coach.image', url)} // Pass the correct URL
                                    path={`teams/${team.id}/coach`}
                                    filename="trainer_foto"
                                    buttonText={"Trainer Bild Hochladen"}
                                />
                            </div>
                        ) : (
                            <div>
                                <span>Kein Trainer Bild Vorhanden</span>
                                <FirebaseImageUpload
                                    onUploadSuccess={(url) => handleInputChange('coach.image', url)} // Pass the correct URL
                                    path={`teams/${team.id}/coach`}
                                    filename="trainer_foto"
                                    buttonText={"Trainer Bild Hochladen"}
                                />
                            </div>
                        )}
                    </Descriptions.Item>

                    <Descriptions.Item label="Points">
                        <InputNumber
                            value={team.points}
                            onChange={(value) => handleInputChange('points', value)}
                            style={{ width: '100%' }}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Average Age">{team.average_age}</Descriptions.Item>
                    <Descriptions.Item label="Membership Since">{team.membership_since}</Descriptions.Item>
                    <Descriptions.Item label="League Code">
                        <Select
                            value={team.league?.id}
                            onChange={handleLeagueChange}
                            style={{ width: '100%' }}
                        >
                            {leagues.map((league) => (
                                <Option key={league.id} value={league.id}>
                                    {league.name}
                                </Option>
                            ))}
                        </Select>
                    </Descriptions.Item>
                    <Descriptions.Item label="Foundation Year">{team.foundation_year}</Descriptions.Item>
                    <Descriptions.Item label="SID">{team.sid}</Descriptions.Item>
                    <Descriptions.Item label="Team Passwort">{team.usrpass}</Descriptions.Item>
                    <Descriptions.Item label="Balance">
                        € {team.balance}
                        <Button type="link" onClick={showTopUpModal}>Guthaben Hinzufügen</Button>
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            {/* Players Card */}
            <Card title="Players" style={{ marginTop: 16 }} extra={
                <Button type="primary" onClick={showAddTeamModal}>
                    Spieler Hinzufügen
                </Button>
            }>
                <Table
                    columns={playerColumns}
                    dataSource={players}
                    pagination={false}
                    rowKey="id"
                />
            </Card>

            {/* Rechnungen Card */}
            <Card title="Rechnungen" style={{ marginTop: 16 }}>
                <Table
                    dataSource={rechnungen}
                    columns={rechnungenColumns}
                    loading={loading}
                    rowKey="id"
                />
            </Card>

            {/* Modals */}
            <AddTeamPlayerModal
                visible={isAddTeamModalVisible}
                onCancel={handleAddTeamModalCancel}
                onCreate={handleAddTeamModalCreate}
                team={team}
            />

            <Modal
                title="Guthaben Hinzufügen"
                visible={isTopUpModalVisible}
                onCancel={handleTopUpModalCancel}
                footer={[
                    <Button key="cancel" onClick={handleTopUpModalCancel}>
                        Cancel
                    </Button>,
                    <Button key="submit" type="primary" onClick={handleTopUp}>
                        Guthaben Hinzufügen
                    </Button>,
                ]}
            >
                <p>Enter the amount to top up:</p>
                <InputNumber
                    min={0}
                    value={topUpAmount}
                    onChange={(value) => setTopUpAmount(value)}
                    style={{ width: '100%' }}
                />
            </Modal>
        </div>
    );
}
