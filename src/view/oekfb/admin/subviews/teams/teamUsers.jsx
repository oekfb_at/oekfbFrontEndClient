import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Table, message, Button, Input } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';
import { SearchOutlined } from '@ant-design/icons';

export default function TeamUserIndex() {
    const [dataSource, setDataSource] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchText, setSearchText] = useState('');
    const history = useHistory();

    useEffect(() => {
        const fetchAllData = async () => {
            const apiService = new ApiService();
            let allUsers = [];
            let page = 1;
            const maxPages = 10;  // Set the maximum number of pages to fetch

            try {
                while (page <= maxPages) {
                    const data = await apiService.get(`users/teams?page=${page}&per=50`);
                    if (data && data.length > 0) {
                        allUsers = allUsers.concat(data);
                        page++;
                    } else {
                        break;  // Stop fetching if no data is returned
                    }
                }

                // Remove duplicates by creating a unique map of users by id
                const uniqueUsers = new Map();
                allUsers.forEach((item) => {
                    if (!uniqueUsers.has(item.id)) {
                        uniqueUsers.set(item.id, {
                            key: item.id,
                            id: item.id,
                            email: item.email,
                            type: item.type,
                            first_name: item.first_name,
                            last_name: item.last_name,
                            password_hash: item.password_hash,
                            teams: item.teams,
                            team_name: item.teams.length > 0 ? item.teams[0].team_name : 'N/A',
                            pass: item.teams.length > 0 ? item.teams[0].usrpass : 'N/A',
                        });
                    }
                });

                const formattedData = Array.from(uniqueUsers.values());

                console.log("Formatted Data:", formattedData); // Debug log
                setDataSource(formattedData);
                setFilteredData(formattedData);
            } catch (error) {
                message.error('Failed to fetch users');
                console.error('Fetch users error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchAllData();
    }, []);

    const setUserDetail = (value) => {
        console.log("Setting user detail in session storage:", value);
        sessionStorage.setItem("userDetail", JSON.stringify(value));
    };

    const handleSearch = (e) => {
        const { value } = e.target;
        setSearchText(value);

        const filtered = dataSource.filter(item => {
            const itemString = JSON.stringify(item).toLowerCase();
            return itemString.includes(value.toLowerCase());
        });

        setFilteredData(filtered);
    };

    const columns = [
        {
            title: 'First Name',
            dataIndex: 'first_name',
        },
        {
            title: 'Last Name',
            dataIndex: 'last_name',
        },

        {
            title: 'Email',
            dataIndex: 'email',
        },
        {
            title: 'Passwort',
            dataIndex: 'pass',
        },
        {
            title: 'Team Name',
            dataIndex: 'team_name',
        },
        {
            title: 'Typ',
            dataIndex: 'type',
        },

        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Button
                    type="primary"
                    onClick={() => {
                        setUserDetail(record);
                        history.push(`/admin/user_detail/${record.id}`);
                    }}
                >
                    Details
                </Button>
            ),
        },
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Mannschaften Login Übersicht</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Mannschaften Login Übersicht" style={{ marginTop: 16 }}>
                <Input
                    placeholder="Suche nach Mannschaft Name, SID oder Email Adresse"
                    value={searchText}
                    onChange={handleSearch}
                    prefix={<SearchOutlined />}
                    style={{ marginBottom: 16 }}
                />
                <Table
                    dataSource={filteredData}
                    columns={columns}
                    loading={loading}
                />
            </Card>
        </div>
    );
}

