import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Breadcrumb, Card, Descriptions, Spin, Input, Button, Select, message, Modal, DatePicker, Table } from 'antd';
import moment from 'moment';  // Import moment.js for date handling
import ApiService from "../../../../../network/ApiService";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";
import TeamRechnungModal from "../../../../pages/invoice/TeamRechnungModal";
import { useHistory } from "react-router-dom";

const { Option } = Select;

const countries = [
    "Afghanistan", "Albanien", "Algerien", "Andorra", "Angola", "Antigua und Barbuda", "Argentinien",
    "Armenien", "Australien", "Österreich", "Aserbaidschan", "Bahamas", "Bahrain", "Bangladesch", "Barbados",
    "Weißrussland", "Belgien", "Belize", "Benin", "Bhutan", "Bolivien", "Bosnien und Herzegowina", "Botswana",
    "Brasilien", "Brunei", "Bulgarien", "Burkina Faso", "Burundi", "Kap Verde", "Kambodscha", "Kamerun",
    "Kanada", "Zentralafrikanische Republik", "Tschad", "Chile", "China", "Kolumbien", "Komoren", "Kongo",
    "Costa Rica", "Kroatien", "Kuba", "Zypern", "Tschechische Republik", "Dänemark", "Dschibuti", "Dominica",
    "Dominikanische Republik", "Ecuador", "Ägypten", "El Salvador", "Äquatorialguinea", "Eritrea", "Estland",
    "Eswatini", "Äthiopien", "Fidschi", "Finnland", "Frankreich", "Gabun", "Gambia", "Georgien", "Deutschland",
    "Ghana", "Griechenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras",
    "Ungarn", "Island", "Indien", "Indonesien", "Iran", "Irak", "Irland", "Israel", "Italien", "Jamaika",
    "Japan", "Jordanien", "Kasachstan", "Kenia", "Kiribati", "Kuwait", "Kirgistan", "Laos", "Lettland",
    "Libanon", "Lesotho", "Liberia", "Libyen", "Liechtenstein", "Litauen", "Luxemburg", "Madagaskar",
    "Malawi", "Malaysia", "Maldiven", "Mali", "Malta", "Marshallinseln", "Mauretanien", "Mauritius",
    "Mexiko", "Mikronesien", "Moldau", "Monaco", "Mongolei", "Montenegro", "Marokko", "Mosambik",
    "Myanmar", "Namibia", "Nauru", "Nepal", "Niederlande", "Neuseeland", "Nicaragua", "Niger",
    "Nigeria", "Nordkorea", "Nordmazedonien", "Norwegen", "Oman", "Pakistan", "Palau", "Palästina",
    "Panama", "Papua-Neuguinea", "Paraguay", "Peru", "Philippinen", "Polen", "Portugal", "Katar",
    "Rumänien", "Russland", "Ruanda", "St. Kitts und Nevis", "St. Lucia", "St. Vincent und die Grenadinen",
    "Samoa", "San Marino", "Sao Tome und Principe", "Saudi-Arabien", "Senegal", "Serbien", "Seychellen",
    "Sierra Leone", "Singapur", "Slowakei", "Slowenien", "Salomonen", "Somalia", "Südafrika",
    "Südkorea", "Südsudan", "Spanien", "Sri Lanka", "Sudan", "Surinam", "Schweden", "Schweiz",
    "Syrien", "Taiwan", "Tadschikistan", "Tansania", "Thailand", "Timor-Leste", "Togo", "Tonga", "Trinidad und Tobago",
    "Tunesien", "Türkei", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "Vereinigte Arabische Emirate",
    "Vereinigtes Königreich", "Vereinigte Staaten", "Uruguay", "Usbekistan", "Vanuatu", "Vatikanstadt", "Venezuela",
    "Vietnam", "Jemen", "Sambia", "Simbabwe"
];

export default function PlayerDetail() {
    const { id } = useParams();
    const [player, setPlayer] = useState(null);
    const [originalPlayer, setOriginalPlayer] = useState(null);
    const [team, setTeam] = useState(null);
    const [loading, setLoading] = useState(true);
    const [teamModalVisible, setTeamModalVisible] = useState(false);
    const [events, setEvents] = useState([]);
    const [selectedTeam, setSelectedTeam] = useState(null);
    const [isDeleteModalVisible, setDeleteModalVisible] = useState(false);
    const [isResetModalVisible, setResetModalVisible] = useState(false);
    const history = useHistory();

    const BASE_URL = "https://oekfb.com";

    useEffect(() => {
        const fetchPlayer = async () => {
            setLoading(true);
            try {
                const playerResponse = await new ApiService().get(`players/internal/${id}`);
                setPlayer(playerResponse);
                setOriginalPlayer(playerResponse);

                if (playerResponse.team && playerResponse.team.id) {
                    const teamResponse = await new ApiService().get(`teams/${playerResponse.team.id}`);
                    setTeam(teamResponse);
                }

                // Fetch Player Events
                const eventResponse = await new ApiService().get(`events/detail/player/${id}`);
                setEvents(eventResponse);

            } catch (error) {
                console.error('Failed to fetch player or team details:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchPlayer();
    }, [id]);

    const handleInputChange = (field, value) => {
        if (field === 'blockdate') {
            if (value) {
                // Manually set the time to 01:00 AM
                const formattedDate = moment(value).set({ hour: 1, minute: 0, second: 0 }).utc().format();
                setPlayer({ ...player, [field]: formattedDate });
            } else {
                // If the blockdate is cleared, set it to null explicitly
                setPlayer({ ...player, [field]: null });
            }
        } else {
            setPlayer({ ...player, [field]: value });
        }
    };

    const hasPlayerChanged = () => {
        return JSON.stringify(player) !== JSON.stringify(originalPlayer);
    };

    const handleUpdatePlayer = async () => {
        if (hasPlayerChanged()) {
            const apiService = new ApiService();
            try {
                await apiService.patch(`players/${player.id}`, player);
                sessionStorage.setItem("playerDetail", JSON.stringify(player));
                setOriginalPlayer(player);
                message.success('Spieler erfolgreich aktualisiert');
            } catch (error) {
                message.error('Fehler beim Aktualisieren des Spielers');
                console.error('Update player error:', error);
            }
        } else {
            message.info('Keine Änderungen zu aktualisieren');
        }
    };

    const handleUpload = async (url, field) => {
        try {
            const updatedPlayer = {
                ...player,
                [field]: url,
            };
            setPlayer(updatedPlayer);
            await new ApiService().patch(`players/${player.id}`, updatedPlayer);
            setOriginalPlayer(updatedPlayer);
            message.success('Datei hochgeladen und Spieler erfolgreich aktualisiert');
        } catch (error) {
            console.error(`Fehler beim Aktualisieren des Spielers mit hochgeladener Datei: ${field}`, error);
            message.error(`Fehler beim Aktualisieren des Spielers mit hochgeladener Datei: ${field}`);
        }
    };

    const handleTeamSelect = async (team) => {
        setSelectedTeam(team);
        setTeamModalVisible(false);

        try {
            const transferResponse = await new ApiService().post('transfers/admin/create', {
                team: team.id,
                player: player.id,
                playerName: player.name,
                teamName: team.team_name || "NO NAME",
                playerImage: player.image,
                teamImage: team.logo
            });

            if (transferResponse.id) {
                window.location.reload();
            }
        } catch (error) {
            console.error('Fehler während des Transferprozesses:', error);
            message.error('Fehler beim Transfer.');
        }
    };

    const handleDeletePlayer = async () => {
        try {
            await new ApiService().delete(`players/${player.id}`);
            message.success("Spieler erfolgreich gelöscht");
            setDeleteModalVisible(false);
            history.push('/admin/players');
        } catch (error) {
            message.error("Fehler beim Löschen des Spielers");
            console.error("Delete player error:", error);
        }
    };

    const handleResetPlayer = async () => {
        try {
            await new ApiService().get(`players/reject/${player.id}`);
            message.success("Spieler erfolgreich zurückgesetzt");
            setResetModalVisible(false);
        } catch (error) {
            message.error("Fehler beim zurücksetzen des Spielers");
            console.error("Delete player error:", error);
        }
    };

    const confirmDeletePlayer = () => {
        Modal.confirm({
            title: 'Sind Sie sicher, dass Sie diesen Spieler löschen möchten?',
            content: 'Diese Aktion kann nicht rückgängig gemacht werden.',
            onOk: handleDeletePlayer,
            okText: 'Löschen',
            cancelText: 'Abbrechen',
            okButtonProps: { danger: true },
        });
    };

    const confirmResetPlayer = () => {
        Modal.confirm({
            title: 'Sind Sie sicher, dass Sie diesen Spieler zurücksetzen möchten?',
            content: 'Diese Aktion kann nicht rückgängig gemacht werden.',
            onOk: handleResetPlayer,
            okText: 'Zurücksetzen',
            cancelText: 'Abbrechen',
            okButtonProps: { danger: true },
        });
    };

    // Table columns for events
    const eventColumns = [
        {
            title: 'Bild',
            dataIndex: 'image',
            key: 'image',
            render: (image) => (
                <img
                    src={image}
                    alt="Player Event"
                    style={{ width: '50px', height: '50px', borderRadius: '5px', objectFit: "contain" }}
                />
            ),
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Spielernummer',
            dataIndex: 'number',
            key: 'number',
        },
        {
            title: 'Event Typ',
            dataIndex: 'type',
            key: 'type',
            render: (type) => {
                const typeMap = {
                    yellowCard: 'Gelbe Karte',
                    yellowRedCard: 'Gelb-Rote Karte',
                    redCard: 'Rote Karte',
                    goal: 'Tor',
                };
                return typeMap[type] || type;
            }
        },
        {
            title: 'Spielminute',
            dataIndex: 'minute',
            key: 'minute',
            render: (minute) => `Minute ${minute}`
        },
        {
            title: 'Aktionen',
            key: 'actions',
            render: (_, record) => (
                <Button
                    type="link"
                    href={`/admin/match_detail/${record.match?.id}`}
                    target="_blank"
                >
                    Zum Spiel
                </Button>
            ),
        }
    ];


    if (loading) {
        return <Spin size="large" />;
    }

    if (!player) {
        return <p>Spieler nicht gefunden</p>;
    }

    return (
        <div style={{ padding: 24 }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/admin/players">Spieler Übersicht</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Spieler Detail</Breadcrumb.Item>
                </Breadcrumb>
                <div>
                    <Button
                        type="primary"
                        onClick={handleUpdatePlayer}
                        disabled={!hasPlayerChanged()}
                        style={{ marginRight: 10 }}
                    >
                        Update
                    </Button>
                    <Button
                        type="danger"
                        onClick={confirmDeletePlayer}
                    >
                        Spieler Löschen
                    </Button>
                </div>
            </div>

            <div style={{ marginTop: 16 }}>
                {team && (
                    <Card title="Team Information" style={{ marginBottom: 16 }}>
                        <div style={{ display: 'flex', alignItems: 'center', marginBottom: 16 }}>
                            <img
                                src={team.logo || `${BASE_URL}/images/default-team.png`}
                                alt="Team Logo"
                                style={{ width: '50px', height: '50px', marginRight: '10px' }}
                            />
                            <span>{team.team_name}</span>
                        </div>
                        <Button type="primary" href={`/admin/team_detail/${team.id}`}>
                            Zur Mannschaft
                        </Button>
                    </Card>
                )}

                <Card title="Spieler Information">
                    <div style={{ width: '100%', maxWidth: "200px", marginBottom: "20px" }}>
                        <img src={player.image} alt="Player" style={{ width: '100%', borderRadius: '10px' }} />
                        <div style={{ marginTop: "20px" }}>
                            <FirebaseImageUpload
                                onUploadSuccess={(url) => handleUpload(url, 'image')}
                                path={`players/${player.id}`}
                                filename="player_image"
                                buttonText={"Spieler Foto Hochladen"}
                            />
                        </div>
                    </div>

                    <Descriptions bordered column={1}>
                        <Descriptions.Item label="Name">
                            <Input
                                value={player.name}
                                onChange={(e) => handleInputChange('name', e.target.value)}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="SID">{player.sid}</Descriptions.Item>
                        <Descriptions.Item label="ID/UUID">{player.id}</Descriptions.Item>
                        <Descriptions.Item label="Email">
                            <Input
                                value={player.email}
                                onChange={(e) => handleInputChange('email', e.target.value)}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="Status">
                            <Select
                                value={player.status}
                                onChange={(value) => handleInputChange('status', value)}
                                style={{ width: '100%' }}
                            >
                                <Option value={true}>Bestätigt</Option>
                                <Option value={false}>Nicht Bestätigt</Option>
                                <Option value={null}>Importiert</Option>
                            </Select>
                        </Descriptions.Item>
                        <Descriptions.Item label="Nationalität">
                            <Select
                                value={player.nationality}
                                onChange={(value) => handleInputChange('nationality', value)}
                                style={{ width: '100%' }}
                            >
                                {countries.map(country => (
                                    <Option key={country} value={country}>
                                        {country}
                                    </Option>
                                ))}
                            </Select>
                        </Descriptions.Item>
                        <Descriptions.Item label="Position">
                            <Select
                                value={player.position}
                                onChange={(value) => handleInputChange('position', value)}
                                style={{ width: '100%' }}
                            >
                                <Option value="Feldspieler">Feldspieler</Option>
                                <Option value="Tormann">Tormann</Option>
                            </Select>
                        </Descriptions.Item>
                        <Descriptions.Item label="Geburtstag">
                            <Input
                                value={player.birthday}
                                onChange={(e) => handleInputChange('birthday', e.target.value)}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="Spielberechtigung">
                            <Select
                                value={player.eligibility}
                                onChange={(value) => handleInputChange('eligibility', value)}
                                style={{ width: '100%' }}
                            >
                                <Option value="Spielberechtigt">Spielberechtigt</Option>
                                <Option value="Gesperrt">Gesperrt</Option>
                                <Option value="Warten">Warten</Option>
                            </Select>
                        </Descriptions.Item>

                        {/* DatePicker only for date, time set to 01:00 AM */}
                        <Descriptions.Item label="Spieler Sperre - Bis">
                            <DatePicker
                                value={player.blockdate ? moment(player.blockdate) : null}
                                onChange={(date) => {
                                    const formattedDate = date ? moment(date).set({ hour: 1, minute: 0, second: 0 }).utc().format() : null;
                                    handleInputChange('blockdate', formattedDate);
                                }}
                                format="YYYY-MM-DD"
                                style={{ width: '100%' }}
                            />
                        </Descriptions.Item>

                        <Descriptions.Item label="Ersatz Spieler">
                            {player.bank === true || player.bank === null ? 'JA' : 'NEIN'}
                        </Descriptions.Item>
                        <Descriptions.Item label="Anmeldedatum">{player.register_date}</Descriptions.Item>
                        <Descriptions.Item label="Ausweis">
                            <FirebaseImageUpload
                                existingUrl={player.identification}
                                path={`players/${player.id}`}
                                filename={`${player.id}`}
                                buttonText={"Ausweis Hochladen"}
                                onUploadSuccess={(url) => handleUpload(url, 'identification')}
                            />
                        </Descriptions.Item>
                    </Descriptions>
                </Card>

                {/* New Card: Player Events */}
                <Card title="Spieler Ereignisse" style={{ marginTop: 16 }}>
                    <Table
                        dataSource={events}
                        columns={eventColumns}
                        rowKey="id"
                        pagination={{ pageSize: 20 }}
                    />
                </Card>


                <Card title="Spieler Transferieren" style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={() => setTeamModalVisible(true)}>
                        Spieler Transferieren
                    </Button>
                </Card>

                {team && (
                    <Card title="Spieler Ablehnen" style={{ marginBottom: 16 }}>
                        <Button
                            style={{ color: "white", backgroundColor: "red", border: "none", marginBottom: "20px" }}
                            type="primary"
                            onClick={() => confirmResetPlayer()}
                        >
                            Spieler Zurücksetzen
                        </Button>
                        <p> Dieser Knopf setzt den Spieler auf Warten und Löscht sein Foto und Ausweis. Der Mannschaftsleiter wird mit einer Email informiert.</p>
                    </Card>
                )}

                <Card title="Spieler Transferieren" style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={() => setTeamModalVisible(true)}>
                        Spieler Transferieren
                    </Button>
                </Card>


                <TeamRechnungModal
                    visible={teamModalVisible}
                    onClose={() => setTeamModalVisible(false)}
                    onTeamSelect={handleTeamSelect}
                />
            </div>
        </div>
    );
}

