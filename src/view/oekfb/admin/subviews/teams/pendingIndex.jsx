import React, { useState, useEffect } from 'react';
import { Breadcrumb, Card, Button, Table } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from "../../../../../network/ApiService";

export default function PendingIndex() {
    const [players, setPlayers] = useState([]);
    const history = useHistory();
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 50,
        total: 0
    });
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        fetchPlayers(pagination.current, pagination.pageSize);
    }, [pagination.current, pagination.pageSize]);

    const fetchPlayers = async (page, pageSize) => {
        setLoading(true);
        try {
            const endpoint = `players/pending?per=${pageSize}&page=${page}`;
            const response = await new ApiService().get(endpoint);

            const items = Array.isArray(response) ? response : response.items || [];
            const metadata = response.metadata || { total: items.length, page };

            setPlayers(items);
            setPagination(prevPagination => ({
                ...prevPagination,
                total: metadata.total,
                current: metadata.page,
            }));
        } catch (error) {
            console.error('Failed to fetch players:', error);
        } finally {
            setLoading(false);
        }
    };

    const setPlayerDetail = (value) => {
        sessionStorage.setItem("playerDetail", JSON.stringify(value));
    };

    const handleTableChange = (pagination) => {
        setPagination({
            ...pagination,
            current: pagination.current,
            pageSize: pagination.pageSize
        });
    };

    const columns = [
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (image) => image ? <img src={image} alt="player" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        { title: 'Name', dataIndex: 'name', key: 'name' },
        { title: 'SID', dataIndex: 'sid', key: 'sid' },
        { title: 'Nationality', dataIndex: 'nationality', key: 'nationality' },
        { title: 'Position', dataIndex: 'position', key: 'position' },
        { title: 'Spielberechtigung', dataIndex: 'eligibility', key: 'eligibility' },
        { title: 'Birthday', dataIndex: 'birthday', key: 'birthday' },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button
                    type="text"
                    className="hp-text-color-primary-1 hp-hover-bg-primary-4"
                    onClick={() => {
                        setPlayerDetail(record);
                        history.push(`/admin/players_detail/${record.id}`);
                    }}
                >
                    Details
                </Button>
            ),
        }
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Spieler Berechtigungen</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Player Index" style={{ marginTop: 16 }}>
                <Table
                    columns={columns}
                    dataSource={players}
                    pagination={pagination}
                    loading={loading}
                    onChange={handleTableChange}
                    rowKey="id"
                />
            </Card>
        </div>
    );
}
