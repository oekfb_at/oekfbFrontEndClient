import React, { useState, useEffect } from 'react';
import { Breadcrumb, Card, Button, Table, Input } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from "../../../../../network/ApiService";

const { Search } = Input;

export default function AdminTeamIndex() {
    const [teams, setTeams] = useState([]);
    const history = useHistory();
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 20,
        total: 0
    });
    const [loading, setLoading] = useState(false);
    const [searchValue, setSearchValue] = useState('');

    useEffect(() => {
        fetchTeams(pagination.current, pagination.pageSize, searchValue);
    }, [pagination.current, pagination.pageSize, searchValue]);

    const fetchTeams = async (page, pageSize, searchValue = '') => {
        setLoading(true);
        try {
            const endpoint = searchValue
                ? `teams/search/${searchValue}`
                : `teams?per=${pageSize}&page=${page}`;
            const response = await new ApiService().get(endpoint);

            console.log('API Response:', response);

            const items = Array.isArray(response) ? response : response.items || [];
            const metadata = response.metadata || { total: items.length, page };

            setTeams(items);
            setPagination(prevPagination => ({
                ...prevPagination,
                total: metadata.total,
                current: metadata.page,
            }));
        } catch (error) {
            console.error('Failed to fetch teams:', error);
        } finally {
            setLoading(false);
        }
    };

    const setTeamDetail = (value) => {
        console.log("Setting Team detail in session storage:", value);
        sessionStorage.setItem("teamDetail", JSON.stringify(value));
    };

    const handleTableChange = (pagination) => {
        setPagination({
            ...pagination,
            current: pagination.current,
            pageSize: pagination.pageSize
        });
    };

    const handleSearch = (value) => {
        setSearchValue(value);
    };

    const columns = [
        {
            title: 'Logo',
            dataIndex: 'logo',
            key: 'logo',
            render: (logo) => logo ? <img src={`${logo}`} alt="logo" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        { title: 'Mannschaft', dataIndex: 'team_name', key: 'team_name' },
        { title: 'Liga', dataIndex: ['league_code'], key: 'league_code', render: (league_code) => league_code || 'N/A' },
        { title: 'Durschn. Alter', dataIndex: 'average_age', key: 'average_age' },
        {
            title: 'Guthaben',
            dataIndex: 'balance',
            key: 'balance',
            render: (text) => `${text} €`,
        },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button
                    type="text"
                    className="hp-text-color-primary-1 hp-hover-bg-primary-4"
                    onClick={() => {
                        setTeamDetail(record);
                        history.push(`/admin/team_detail/${record.id}`);
                    }}
                >
                    Details
                </Button>
            ),
        }
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Mannschaften Übersicht</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Mannschaften Übersicht" style={{ marginTop: 16 }}>
                <Search
                    placeholder="Suche nach einer Mannschaft per Name oder SID (A-a ! wichtig)"
                    onSearch={handleSearch}
                    enterButton
                    style={{ marginBottom: 16 }}
                />
                <Table
                    columns={columns}
                    dataSource={teams}
                    pagination={pagination}
                    loading={loading}
                    onChange={handleTableChange}
                    rowKey="id"
                />
            </Card>
        </div>
    );
}
