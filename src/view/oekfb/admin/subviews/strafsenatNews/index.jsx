import React, { useState, useEffect } from 'react';
import { Breadcrumb, Card, Button, Table, Modal, Form, Input } from 'antd';
import { useHistory } from 'react-router-dom';
import { RiCloseFill } from 'react-icons/ri';
import ApiService from "../../../../../network/ApiService";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";

const { confirm } = Modal;

export default function AdminStrafsenatNewsIndex() {
    const [news, setNews] = useState([]);
    const [visible, setVisible] = useState(false);
    const [imageUrl, setImageUrl] = useState('');
    const [leagues, setLeagues] = useState([]);
    const history = useHistory();
    const apiService = new ApiService();
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 25,
        total: 0
    });
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();

    useEffect(() => {
        fetchNews(pagination.current, pagination.pageSize);
        fetchLeagues();
    }, [pagination.current, pagination.pageSize]);

    const fetchNews = async (page, pageSize) => {
        setLoading(true);
        try {
            const endpoint = `news/strafsenat?per=${pageSize}&page=${page}`;
            const response = await apiService.get(endpoint);
            const items = Array.isArray(response) ? response : response.items || [];
            const total = response.metadata ? response.metadata.total : items.length;

            setNews(items);
            setPagination(prevPagination => ({
                ...prevPagination,
                total,
                current: page,
            }));
        } catch (error) {
            console.error('Failed to fetch news:', error);
        } finally {
            setLoading(false);
        }
    };

    const fetchLeagues = async () => {
        try {
            const response = await apiService.get('leagues?per=200');
            const leagueItems = Array.isArray(response.items) ? response.items : [];
            setLeagues(leagueItems);
        } catch (error) {
            console.error('Failed to fetch leagues:', error);
        }
    };

    const handleTableChange = (pagination) => {
        setPagination({
            ...pagination,
            current: pagination.current,
            pageSize: pagination.pageSize
        });
    };

    const handleUploadSuccess = (url) => {
        setImageUrl(url);
    };

    const handleCreateNews = async () => {
        try {
            const values = await form.validateFields();
            const postData = {
                text: values.text,
                image: imageUrl,
                tag: "strafsenat",
                title: values.title,
            };

            const response = await apiService.post('news', postData);

            if (response) {
                alert('News created successfully!');
                form.resetFields();
                setVisible(false);
                fetchNews(pagination.current, pagination.pageSize);
            } else {
                throw new Error('Failed to create news. Please try again.');
            }
        } catch (errorInfo) {
            console.log('Error occurred:', errorInfo);
            alert('An error occurred while creating the news. Please check your inputs and try again.');
        }
    };

    const handleDelete = async (id) => {
        try {
            const response = await apiService.delete(`news/${id}`);
            alert('News deleted successfully!');
            fetchNews(pagination.current, pagination.pageSize);
        } catch (error) {
            console.error('Error deleting news:', error);
            alert('An error occurred while deleting the news.');
        }
    };

    const showDeleteConfirm = (record) => {
        confirm({
            title: 'Sind Sie sicher das Sie diesen Artikel löschen möchten?',
            content: `Artikel: ${record.title}`,
            okText: 'Ja',
            okType: 'danger',
            cancelText: 'Nein',
            onOk() {
                handleDelete(record.id);
            }
        });
    };

    const columns = [
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (image) => image ? <img src={image} alt="news" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        {
            title: 'Titel',
            dataIndex: 'title',
            key: 'title',
            render: (title) => (
                <div style={{
                    maxWidth: '150px',
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    WebkitLineClamp: 2,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'normal'
                }}>
                    {title}
                </div>
            )
        },
        {
            title: 'Text',
            dataIndex: 'text',
            key: 'text',
            render: (text) => (
                <div style={{
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    WebkitLineClamp: 3,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    maxWidth: '400px',
                    whiteSpace: 'normal'
                }}>
                    {text}
                </div>
            )
        },
        {
            title: 'Tag',
            dataIndex: 'tag',
            key: 'tag'
        },
        {
            title: 'Created',
            dataIndex: 'created',
            key: 'created',
            render: (created) => {
                const date = new Date(created);
                const formattedDate = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}, ${date.getHours()}:${date.getMinutes().toString().padStart(2, '0')}`;
                return formattedDate;
            }
        },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <>
                    <Button
                        type="text"
                        className="hp-text-color-primary-1 hp-hover-bg-primary-4"
                        onClick={() => {
                            sessionStorage.setItem("strafsenat_newsDetail", JSON.stringify(record));
                            history.push(`/admin/news_strafsenat_detail/${record.id}`);
                        }}
                    >
                        Details
                    </Button>
                    <Button
                        type="danger"
                        onClick={() => showDeleteConfirm(record)}
                    >
                        Löschen
                    </Button>
                </>
            ),
        }
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/admin/news_strafsenat">Strafsenat News</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Strafsenat News Detail</Breadcrumb.Item>
            </Breadcrumb>

            <div style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: 16 }}>
                <Button
                    type="primary"
                    onClick={() => setVisible(true)}
                >
                    Neuen Strafnsenat Artikel Erstellen
                </Button>
            </div>

            <Card title="Strafsenat Artikel Übersicht" style={{ marginTop: 16 }}>
                <Table
                    columns={columns}
                    dataSource={news}
                    pagination={pagination}
                    loading={loading}
                    onChange={handleTableChange}
                    rowKey="id"
                />
            </Card>

            <Modal
                title="Neuen Strafnsenat Artikel Erstellen"
                width={400}
                visible={visible}
                onCancel={() => setVisible(false)}
                footer={[
                    <Button key="cancel" onClick={() => setVisible(false)}>
                        Cancel
                    </Button>,
                    <Button key="submit" type="primary" onClick={handleCreateNews}>
                        Create
                    </Button>,
                ]}
                closeIcon={<RiCloseFill className="remix-icon text-color-black-100" size={24} />}
            >
                <Form form={form} layout="vertical" name="createNewsForm">
                    <Form.Item
                        label="Titel"
                        name="title"
                        rules={[{ required: true, message: 'Please input the title!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Inhalt"
                        name="text"
                        rules={[{ required: true, message: 'Please input the news text!' }]}
                    >
                        <Input.TextArea rows={4} />
                    </Form.Item>
                    <Form.Item label="Feature Bild">
                        <FirebaseImageUpload
                            onUploadSuccess={handleUploadSuccess}
                            path={`news/${Date.now()}`}
                            filename="news-image"
                            existingUrl={imageUrl}
                            buttonText="Bild Hochladen"
                        />
                    </Form.Item>
                    <Form.Item label="Tag">
                        <Input value="strafsenat" disabled />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
