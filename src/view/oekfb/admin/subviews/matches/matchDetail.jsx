import React, { useState, useEffect } from "react";
import {
  Breadcrumb,
  Card,
  Descriptions,
  Spin,
  Input,
  Button,
  Select,
  message,
  DatePicker,
  Table,
  InputNumber,
  Modal,
  Form,
  Checkbox,
} from "antd";
import ApiService from "../../../../../network/ApiService";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";
import moment from "moment";
import { useParams, useHistory } from "react-router-dom";
import MatchController from "../../../../../network/MatchController";

const { Option } = Select;

export default function MatchDetail() {
  const { id } = useParams();
  const history = useHistory(); // if you want to redirect after deleting
  const [match, setMatch] = useState(null);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isDeleteModalVisible, setIsDeleteModalVisible] = useState(false);
  const [loading, setLoading] = useState(true);
  const [stadiums, setStadiums] = useState([]);
  const [referee, setReferee] = useState(null);
  const [season, setSeason] = useState(null);
  const [homeTeam, setHomeTeam] = useState(null);
  const [awayTeam, setAwayTeam] = useState(null);
  const [players, setPlayers] = useState({});
  const [eventForm] = Form.useForm();

  const apiService = new ApiService();
  const matchController = new MatchController();

  useEffect(() => {
    const fetchMatchData = async () => {
      setLoading(true);
      try {
        if (id) {
          const matchResponse = await apiService.get(`matches/${id}`);
          if (matchResponse) {
            setMatch(matchResponse);

            // Fetch referee data
            if (matchResponse.referee && matchResponse.referee.id) {
              const refereeResponse = await apiService.get(
                `referees/${matchResponse.referee.id}`
              );
              setReferee(refereeResponse);
            }

            // Fetch season data
            if (matchResponse.season && matchResponse.season.id) {
              const seasonResponse = await apiService.get(
                `seasons/${matchResponse.season.id}`
              );
              setSeason(seasonResponse);
            }

            // Fetch home team data using team ID
            if (matchResponse.home_team && matchResponse.home_team.id) {
              const homeTeamResponse = await apiService.get(
                `teams/${matchResponse.home_team.id}`
              );
              setHomeTeam(homeTeamResponse);
            }

            // Fetch away team data using team ID
            if (matchResponse.away_team && matchResponse.away_team.id) {
              const awayTeamResponse = await apiService.get(
                `teams/${matchResponse.away_team.id}`
              );
              setAwayTeam(awayTeamResponse);
            }

            // Fetch stadiums
            const stadiumsResponse = await apiService.get("stadiums");
            setStadiums(stadiumsResponse.items || []);
          } else {
            console.error("No match data returned from API.");
          }
        } else {
          console.error("No match ID found in route parameters");
        }
      } catch (error) {
        console.error("Failed to fetch match details:", error.message);
      } finally {
        setLoading(false);
      }
    };
    fetchMatchData();
  }, [id]);

  // Update local state for top-level match fields
  const handleInputChange = (field, value) => {
    setMatch({
      ...match,
      [field]: value,
    });
  };

  // Update local state for nested 'details'
  const handleDetailsInputChange = (field, value) => {
    setMatch({
      ...match,
      details: {
        ...match.details,
        [field]: value,
      },
    });
  };

  // Update local state for nested 'score'
  const handleScoreInputChange = (field, value) => {
    setMatch({
      ...match,
      score: {
        ...match.score,
        [field]: value,
      },
    });
  };

  // This function will PATCH the entire match object to the server
  const handleUpload = async () => {
    try {
      await apiService.patch(`matches/${match.id}`, match);
      message.success("Datei hochgeladen und Spiel erfolgreich aktualisiert");
    } catch (error) {
      console.error("Fehler beim Hochladen der Datei", error.message);
      message.error("Fehler beim Hochladen der Datei");
    }
  };

  // ---- Delete Match Handler ----
  const handleDeleteMatch = async () => {
    try {
      await apiService.delete(`matches/${match.id}`);
      message.success("Match erfolgreich gelöscht");
      setIsDeleteModalVisible(false);

      // Redirect to season detail using the season ID
      const seasonId = match.season?.id;
      if (seasonId) {
        history.push(`/admin/season_detail/${seasonId}`);
      } else {
        message.warning("Kein Saison-ID gefunden, zurück zur Übersicht");
        history.push("/admin/matches");
      }
    } catch (error) {
      message.error("Fehler beim Löschen des Matches");
      console.error("Fehler beim Löschen des Matches:", error.message);
    }
  };

  const getPlayerById = async (id) => {
    if (players[id]) {
      return players[id];
    }

    try {
      const playerResponse = await apiService.get(`players/${id}`);
      setPlayers((prevPlayers) => ({
        ...prevPlayers,
        [id]: playerResponse,
      }));
      return playerResponse;
    } catch (error) {
      console.error(
        `Fehler beim Abrufen des Spielers mit ID: ${id}`,
        error.message
      );
      return null;
    }
  };

  // Player table columns
  const playerColumns = [
    {
      title: "Nummer",
      dataIndex: "number",
      key: "number",
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Gelbe Karten",
      dataIndex: "yellow_card",
      key: "yellow_card",
    },
    {
      title: "Rote Karten",
      dataIndex: "red_card",
      key: "red_card",
    },
    {
      title: "Rot-Gelbe Karten",
      dataIndex: "red_yellow_card",
      key: "red_yellow_card",
    },
    {
      title: "Spieler Bild",
      dataIndex: "image",
      key: "image",
      render: (image) =>
        image ? (
          <img src={image} alt="Spieler" style={{ maxWidth: "50px" }} />
        ) : (
          "Kein Bild"
        ),
    },
  ];

  const handleAddEvent = async (values) => {
    try {
      const { playerId, minute, eventType, scoreTeam, ownGoal } = values;
      const player = match.home_blanket.players
        .concat(match.away_blanket.players)
        .find((p) => p.id === playerId);

      const playerName = player.name;
      const playerImage = player.image;
      const playerNumber = player.number.toString();
      const teamId = match.home_blanket.players.includes(player)
        ? match.home_team.id
        : match.away_team.id;
      const minuteInt = parseInt(minute, 10);

      let response;
      switch (eventType) {
        case "goal":
          response = await matchController.addGoal(
            id,
            playerId,
            scoreTeam,
            minuteInt,
            playerName,
            playerImage,
            playerNumber,
            ownGoal
          );
          break;
        case "yellow_card":
          response = await matchController.addYellowCard(
            id,
            playerId,
            teamId,
            minuteInt,
            playerName,
            playerImage,
            playerNumber
          );
          break;
        case "red_card":
          response = await matchController.addRedCard(
            id,
            playerId,
            teamId,
            minuteInt,
            playerName,
            playerImage,
            playerNumber
          );
          break;
        case "yellow_red_card":
          response = await matchController.addYellowRedCard(
            id,
            playerId,
            teamId,
            minuteInt,
            playerName,
            playerImage,
            playerNumber
          );
          break;
        default:
          message.error("Unbekannter Ereignistyp");
          return;
      }

      // Refetch the match data to update the UI
      const updatedMatchDetails = await apiService.get(`matches/${id}`);
      setMatch(updatedMatchDetails);
      message.success("Ereignis erfolgreich hinzugefügt");
      setIsModalVisible(false);
      eventForm.resetFields();
    } catch (error) {
      message.error("Ereignis hinzufügen fehlgeschlagen");
      console.error("Fehler beim Hinzufügen des Ereignisses:", error.message);
    }
  };

  const handleDeleteEvent = (eventId) => {
    Modal.confirm({
      title: "Sind Sie sicher, dass Sie dieses Ereignis löschen möchten?",
      content: "Diese Aktion kann nicht rückgängig gemacht werden.",
      okText: "Ja, löschen",
      okType: "danger",
      cancelText: "Abbrechen",
      onOk: async () => {
        try {
          await apiService.delete(`events/${eventId}`);
          message.success("Ereignis erfolgreich gelöscht");

          // Refetch the match data to update the UI
          const updatedMatchDetails = await apiService.get(`matches/${id}`);
          setMatch(updatedMatchDetails);
        } catch (error) {
          message.error("Fehler beim Löschen des Ereignisses");
          console.error("Fehler beim Löschen des Ereignisses:", error.message);
        }
      },
    });
  };

  const eventColumns = [
    {
      title: "Minute",
      dataIndex: "minute",
      key: "minute",
    },
    {
      title: "Art",
      dataIndex: "type",
      key: "type",
    },
    {
      title: "Eigentor",
      dataIndex: "ownGoal",
      key: "ownGoal",
    },
    {
      title: "Spieler Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Spieler Nummer",
      dataIndex: "number",
      key: "number",
    },
    {
      title: "Spieler Bild",
      dataIndex: "image",
      key: "image",
      render: (image) =>
        image ? (
          <img src={image} alt="Spieler" style={{ maxWidth: "50px" }} />
        ) : (
          "Kein Bild"
        ),
    },
    {
      title: "Aktionen",
      key: "action",
      render: (text, record) => (
        <Button type="danger" onClick={() => handleDeleteEvent(record.id)}>
          Löschen
        </Button>
      ),
    },
  ];

  const handleToggleDress = async (teamType) => {
    try {
      await matchController.toggleDress(id, teamType);
      message.success("Trikot erfolgreich gewechselt");

      // Refetch the match data to update the UI
      const updatedMatch = await apiService.get(`matches/${id}`);
      setMatch(updatedMatch);
    } catch (error) {
      message.error("Fehler beim Umschalten des Trikots");
      console.error("Trikot Umschalten Fehler:", error.message);
    }
  };

  if (loading) {
    return <Spin size="large" />;
  }

  if (!match) {
    return <p>Spiel nicht gefunden</p>;
  }

  return (
    <div style={{ padding: 24 }}>
      {/* Breadcrumb */}
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/admin/">Verwaltung</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href={`/admin/season_detail/${season?.id || ""}`}>Liga</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/admin/matches">Spiel Übersicht</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>Spiel Details</Breadcrumb.Item>
        </Breadcrumb>
        <Button
          type="primary"
          onClick={handleUpload}
          style={{ marginRight: 20 }}
        >
          Update
        </Button>
      </div>

      {/* Match Information */}
      <div style={{ marginTop: 16 }}>
        <Card title="Spiel Informationen">
          <Descriptions bordered column={1}>
            <Descriptions.Item label="ID/UUID">{match.id}</Descriptions.Item>
            <Descriptions.Item label="Datum">
              <DatePicker
                showTime
                value={
                  match.details?.date
                    ? moment.parseZone(match.details.date)
                    : null
                }
                onChange={(date) =>
                  handleDetailsInputChange(
                    "date",
                    date ? date.utc().format() : null
                  )
                }
                style={{ width: "100%" }}
              />
            </Descriptions.Item>
            <Descriptions.Item label="Spieltag">
              <InputNumber
                value={match.details?.gameday}
                onChange={(value) => handleDetailsInputChange("gameday", value)}
                style={{ width: "100%" }}
              />
            </Descriptions.Item>
            <Descriptions.Item label="Stadion">
              <Select
                value={match.details?.stadium}
                onChange={(value) => handleDetailsInputChange("stadium", value)}
                style={{ width: "100%" }}
              >
                {stadiums.map((stadium) => (
                  <Option key={stadium.id} value={stadium.id}>
                    {stadium.name}
                  </Option>
                ))}
              </Select>
            </Descriptions.Item>
            <Descriptions.Item label="Status">
              <Select
                value={match.status}
                onChange={(value) => handleInputChange("status", value)}
                style={{ width: "100%" }}
              >
                <Option value="pending">Spielvorschau</Option>
                <Option value="first">1. Halbzeit</Option>
                <Option value="halftime">Halbzeit</Option>
                <Option value="second">2. Halbzeit</Option>
                <Option value="completed">Spielbericht Offen</Option>
                <Option value="submitted">Spiel Eingereicht</Option>
                <Option value="done">Spiel Abgeschlossen und Verrechnet</Option>
                <Option value="cancelled">Spiel Abgesagt</Option>
              </Select>
            </Descriptions.Item>

            <Descriptions.Item label="Bezahlt">
              <Select
                value={match.paid}
                onChange={(value) => handleInputChange("paid", value)}
                style={{ width: "100%" }}
              >
                <Option value={true}>Ja</Option>
                <Option value={false}>Nein</Option>
              </Select>
            </Descriptions.Item>
            <Descriptions.Item label="Ergebnis">
              Heim:{" "}
              <InputNumber
                value={match.score?.home}
                onChange={(value) => handleScoreInputChange("home", value)}
                style={{ width: "100px" }}
              />{" "}
              - Auswärts:{" "}
              <InputNumber
                value={match.score?.away}
                onChange={(value) => handleScoreInputChange("away", value)}
                style={{ width: "100px" }}
              />
            </Descriptions.Item>
            <Descriptions.Item label="Erste Halbzeit Start">
              <DatePicker
                showTime
                value={
                  match.first_half_start_date
                    ? moment.parseZone(match.first_half_start_date)
                    : null
                }
                onChange={(date) =>
                  handleInputChange(
                    "first_half_start_date",
                    date ? date.utc().format() : null
                  )
                }
                style={{ width: "100%" }}
              />
            </Descriptions.Item>

            <Descriptions.Item label="Erste Halbzeit Ende">
              <DatePicker
                showTime
                value={
                  match.first_half_end_date
                    ? moment.parseZone(match.first_half_end_date)
                    : null
                }
                onChange={(date) =>
                  handleInputChange(
                    "first_half_end_date",
                    date ? date.utc().format() : null
                  )
                }
                style={{ width: "100%" }}
              />
            </Descriptions.Item>

            <Descriptions.Item label="Zweite Halbzeit Start">
              <DatePicker
                showTime
                value={
                  match.second_half_start_date
                    ? moment.parseZone(match.second_half_start_date)
                    : null
                }
                onChange={(date) =>
                  handleInputChange(
                    "second_half_start_date",
                    date ? date.utc().format() : null
                  )
                }
                style={{ width: "100%" }}
              />
            </Descriptions.Item>

            <Descriptions.Item label="Zweite Halbzeit Ende">
              <DatePicker
                showTime
                value={
                  match.second_half_end_date
                    ? moment.parseZone(match.second_half_end_date)
                    : null
                }
                onChange={(date) =>
                  handleInputChange(
                    "second_half_end_date",
                    date ? date.utc().format() : null
                  )
                }
                style={{ width: "100%" }}
              />
            </Descriptions.Item>

            <Descriptions.Item label="Schiedsrichter">
              {referee?.name || match.referee?.id}
            </Descriptions.Item>
            <Descriptions.Item label="Saison">
              {season?.name || match.season?.id}
            </Descriptions.Item>
            <Descriptions.Item label="Schiedsrichter Bericht">
              <Input.TextArea
                value={match.bericht || ""}
                onChange={(e) => handleInputChange("bericht", e.target.value)}
                rows={6}
                placeholder="Bericht hier eingeben"
                style={{ width: "100%" }}
              />
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </div>

      {/* Home Team Information */}
      <div style={{ marginTop: 16 }}>
        <Card title="Heim Team Informationen">
          <Descriptions bordered column={1}>
            <Descriptions.Item label="Team Name">
              {match.home_blanket?.name}
            </Descriptions.Item>
            <Descriptions.Item label="Trainer Name">
              {match.home_blanket?.coach?.name}
            </Descriptions.Item>
            <Descriptions.Item label="Trainer Bild">
              {match.home_blanket?.coach?.image_url ? (
                <img
                  src={match.home_blanket.coach.image_url}
                  alt="Trainer"
                  style={{ maxWidth: "100px" }}
                />
              ) : (
                "Kein Bild"
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Logo">
              {match.home_blanket?.logo ? (
                <img
                  src={match.home_blanket.logo}
                  alt="Team Logo"
                  style={{ maxWidth: "100px" }}
                />
              ) : (
                "Kein Logo"
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Trikot">
              {homeTeam?.trikot?.home ? (
                <>
                  <img
                    src={match.home_blanket?.dress}
                    alt="Team Trikot"
                    style={{ maxWidth: "100px" }}
                  />
                  <div>
                    <Button onClick={() => handleToggleDress("home")}>
                      Trikot Wechseln
                    </Button>
                    <p>Die Änderung wird automatisch gespeichert.</p>
                  </div>
                </>
              ) : (
                "Kein Trikot"
              )}
            </Descriptions.Item>
          </Descriptions>
          <Table
            columns={playerColumns}
            dataSource={match.home_blanket?.players || []}
            rowKey="id"
            pagination={false}
          />
        </Card>
      </div>

      {/* Away Team Information */}
      <div style={{ marginTop: 16 }}>
        <Card title="Auswärts Team Informationen">
          <Descriptions bordered column={1}>
            <Descriptions.Item label="Team Name">
              {match.away_blanket?.name}
            </Descriptions.Item>
            <Descriptions.Item label="Trainer Name">
              {match.away_blanket?.coach?.name}
            </Descriptions.Item>
            <Descriptions.Item label="Trainer Bild">
              {match.away_blanket?.coach?.image_url ? (
                <img
                  src={match.away_blanket.coach.image_url}
                  alt="Trainer"
                  style={{ maxWidth: "100px" }}
                />
              ) : (
                "Kein Bild"
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Logo">
              {match.away_blanket?.logo ? (
                <img
                  src={match.away_blanket.logo}
                  alt="Team Logo"
                  style={{ maxWidth: "100px" }}
                />
              ) : (
                "Kein Logo"
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Trikot">
              {awayTeam?.trikot?.away ? (
                <>
                  <img
                    src={match.away_blanket?.dress}
                    alt="Team Trikot"
                    style={{ maxWidth: "100px" }}
                  />
                  <div>
                    <Button onClick={() => handleToggleDress("away")}>
                      Trikot Wechseln
                    </Button>
                    <p>Die Änderung wird automatisch gespeichert.</p>
                  </div>
                </>
              ) : (
                "Kein Trikot"
              )}
            </Descriptions.Item>
          </Descriptions>
          <Table
            columns={playerColumns}
            dataSource={match.away_blanket?.players || []}
            rowKey="id"
            pagination={false}
          />
        </Card>
      </div>

      {/* Match Events */}
      <div style={{ marginTop: 16 }}>
        <Card title="Spiel Ereignisse">
          <Table
            columns={eventColumns}
            dataSource={match.events || []}
            rowKey="id"
            pagination={false}
          />
        </Card>

        <div style={{ marginTop: 16 }}>
          <Button
            type="primary"
            onClick={() => setIsModalVisible(true)}
            style={{ marginRight: 20 }}
          >
            Ereignis hinzufügen
          </Button>

          {/* Trigger the delete modal */}
          <Button
            type="primary"
            onClick={() => setIsDeleteModalVisible(true)}
            style={{ backgroundColor: "red" }}
          >
            Match Löschen
          </Button>
        </div>

        {/* Modal for Adding Event */}
        <Modal
          title="Spiel Ereignis hinzufügen"
          visible={isModalVisible}
          onCancel={() => setIsModalVisible(false)}
          onOk={() => eventForm.submit()}
        >
          <Form form={eventForm} layout="vertical" onFinish={handleAddEvent}>
            <Form.Item
              label="Spieler"
              name="playerId"
              rules={[{ required: true, message: "Bitte Spieler auswählen" }]}
            >
              <Select placeholder="Spieler auswählen">
                {match.home_blanket?.players?.map((player) => (
                  <Option key={player.id} value={player.id}>
                    {`${player.name} (Nr. ${player.number}) - Heim Team`}
                  </Option>
                ))}
                {match.away_blanket?.players?.map((player) => (
                  <Option key={player.id} value={player.id}>
                    {`${player.name} (Nr. ${player.number}) - Auswärts Team`}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item
              label="Minute"
              name="minute"
              rules={[{ required: true, message: "Bitte Minute eingeben" }]}
            >
              <Input type="number" placeholder="Minute" />
            </Form.Item>
            <Form.Item
              label="Ereignisart"
              name="eventType"
              rules={[
                { required: true, message: "Bitte Ereignisart auswählen" },
              ]}
            >
              <Select placeholder="Ereignisart auswählen">
                <Option value="goal">Tor</Option>
                <Option value="yellow_card">Gelbe Karte</Option>
                <Option value="red_card">Rote Karte</Option>
                <Option value="yellow_red_card">Rot-Gelbe Karte</Option>
              </Select>
            </Form.Item>
            <Form.Item shouldUpdate>
              {({ getFieldValue }) =>
                getFieldValue("eventType") === "goal" && (
                  <>
                    <Form.Item
                      label="Tor für"
                      name="scoreTeam"
                      rules={[
                        { required: true, message: "Bitte Team auswählen" },
                      ]}
                    >
                      <Select>
                        <Option value="home">Heim Team</Option>
                        <Option value="away">Auswärts Team</Option>
                      </Select>
                    </Form.Item>
                    <Form.Item name="ownGoal" valuePropName="checked">
                      <Checkbox>Eigentor</Checkbox>
                    </Form.Item>
                  </>
                )
              }
            </Form.Item>
          </Form>
        </Modal>

        {/* Modal for Deleting Match */}
        <Modal
          title="Match löschen"
          visible={isDeleteModalVisible}
          onCancel={() => setIsDeleteModalVisible(false)}
          onOk={handleDeleteMatch}
          okText="Ja, löschen"
          okType="danger"
          cancelText="Abbrechen"
        >
          <p>
            Sind Sie sicher, dass Sie dieses Spiel endgültig löschen möchten?
          </p>
        </Modal>
      </div>
    </div>
  );
}
