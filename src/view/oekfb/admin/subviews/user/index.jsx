import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Table, message, Button } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';

/**
 * UserIndex component fetches and displays a list of users.
 * Users can navigate to the UserDetail component by clicking on the Details button.
 *
 * @component
 */
export default function UserIndex() {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    useEffect(() => {
        /**
         * Fetches the user data from the API and sets it in the state.
         */
        const fetchData = async () => {
            const apiService = new ApiService();
            try {
                const data = await apiService.get('users?per=30');
                if (data && data.items) {
                    const formattedData = data.items.map((item, index) => ({
                        key: index,
                        id: item.id,
                        email: item.email,
                        type: item.type,
                        first_name: item.first_name,
                        last_name: item.last_name,
                        password_hash: item.password_hash
                    }));
                    setDataSource(formattedData);
                }
            } catch (error) {
                message.error('Failed to fetch users');
                console.error('Fetch users error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, []);

    /**
     * Sets the user detail in session storage.
     *
     * @param {Object} value - The user detail to be stored.
     */
    const setUserDetail = (value) => {
        console.log("Setting user detail in session storage:", value);
        sessionStorage.setItem("userDetail", JSON.stringify(value));
    };

    const columns = [
        {
            title: 'First Name',
            dataIndex: 'first_name',
        },
        {
            title: 'Last Name',
            dataIndex: 'last_name',
        },
        {
            title: 'ID',
            dataIndex: 'id',
        },
        {
            title: 'Email',
            dataIndex: 'email',
        },
        {
            title: 'Type',
            dataIndex: 'type',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Button
                    type="primary"
                    onClick={() => {
                        setUserDetail(record);
                        history.push(`/admin/user_detail/${record.id}`);
                    }}
                >
                    Details
                </Button>
            ),
        },
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/admin/user">User</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>User Index</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="User Index" style={{ marginTop: 16 }}>
                <Table dataSource={dataSource.filter(item => item.type === "admin")} columns={columns} loading={loading} />
            </Card>
        </div>
    );
}
