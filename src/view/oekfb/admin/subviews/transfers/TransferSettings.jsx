import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Form, Input, Button, message, DatePicker, Switch } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';
import moment from 'moment';

export default function TransferSettings() {
    const [transferSettings, setTransferSettings] = useState(null);
    const [loading, setLoading] = useState(true);
    const [form] = Form.useForm();
    const history = useHistory();

    useEffect(() => {
        const fetchTransferSettings = async () => {
            const apiService = new ApiService();
            try {
                const data = await apiService.get('transferSettings/settings');
                setTransferSettings(data);
                form.setFieldsValue({
                    is_transfer_open: data.is_transfer_open,
                    name: data.name,
                    from_date: data.from_date ? moment(data.from_date) : null,
                    to: data.to ? moment(data.to) : null,
                });
            } catch (error) {
                message.error('Fehler beim Abrufen der Transfer-Einstellungen');
                console.error('Fetch transfer settings error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchTransferSettings();
    }, [form]);

    const onFinish = async (values) => {
        const formattedValues = {
            ...values,
            from_date: values.from_date ? values.from_date.format('YYYY-MM-DD') : undefined,
            to: values.to ? values.to.format('YYYY-MM-DD') : undefined,
            created: transferSettings.created, // Include created from state
            id: transferSettings.id
        };

        const apiService = new ApiService();
        try {
            await apiService.patch(`transferSettings/${transferSettings.id}`, formattedValues);  // Using settings id in URL
            message.success('Transfer-Einstellungen erfolgreich aktualisiert');
            history.push('/admin/transfer-settings');
        } catch (error) {
            message.error('Fehler beim Aktualisieren der Transfer-Einstellungen');
            console.error('Update transfer settings error:', error);
        }
    };

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Transfer Einstellungen</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Transfer Einstellungen" style={{ marginTop: 16 }}>
                {loading ? (
                    <p>Lädt...</p>
                ) : (
                    <Form
                        form={form}
                        onFinish={onFinish}
                        layout="vertical"
                    >
                        <Form.Item
                            name="is_transfer_open"
                            label={
                                transferSettings?.is_transfer_open
                                    ? "Transfer Fenster Aktuell: OFFEN"
                                    : "Transfer Fenster Aktuell: GESCHLOSSEN"
                            }
                            valuePropName="checked"
                        >
                            <Switch
                                defaultChecked={transferSettings?.is_transfer_open}
                                onChange={(checked) =>
                                    form.setFieldsValue({
                                        is_transfer_open: checked
                                    })
                                }
                            />
                        </Form.Item>
                        <Form.Item name="name" label="Name des Transfer-Fensters" rules={[{ required: true, message: 'Bitte geben Sie den Namen des Transfer-Fensters ein!' }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="from_date" label="Von Datum" rules={[{ required: true, message: 'Bitte wählen Sie das Startdatum aus!' }]}>
                            <DatePicker format="YYYY-MM-DD" />
                        </Form.Item>
                        <Form.Item name="to" label="Bis Datum" rules={[{ required: true, message: 'Bitte wählen Sie das Enddatum aus!' }]}>
                            <DatePicker format="YYYY-MM-DD" />
                        </Form.Item>
                        <Form.Item
                            name="is_dress_change_open"
                            label={
                                transferSettings?.is_dress_change_open
                                    ? "Ist Trikot Tausch möglich: JA"
                                    : "Ist Trikot Tausch möglich: NEIN"
                            }
                            valuePropName="checked"
                        >
                            <Switch
                                defaultChecked={transferSettings?.is_dress_change_open}
                                onChange={(checked) =>
                                    form.setFieldsValue({
                                        is_dress_change_open: checked
                                    })
                                }
                            />
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                Aktualisieren
                            </Button>
                        </Form.Item>
                    </Form>
                )}
            </Card>
        </div>
    );
}
