import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Table, message, Button, Tag } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';
import moment from 'moment';

/**
 * TransferIndex component fetches and displays a list of transfers.
 * Users can navigate to the TransferDetail component by clicking on the Details button.
 *
 * @component
 */
export default function TransferIndex() {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    useEffect(() => {
        /**
         * Fetches the transfer data from the API and sets it in the state.
         */
        const fetchData = async () => {
            const apiService = new ApiService();
            try {
                const data = await apiService.get('transfers?per=300');
                if (data && data.items) {
                    setDataSource(data.items);
                }
            } catch (error) {
                message.error('Failed to fetch transfers');
                console.error('Fetch transfers error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, []);

    const setPlayerDetail = (value) => {
        sessionStorage.setItem("playerDetail", JSON.stringify(value));
    };

    const getStatusTag = (status) => {
        let color;
        switch (status) {
            case 'warten':
                color = 'orange';
                break;
            case 'abgelent':
            case 'abgelaufen':
                color = 'red';
                break;
            case 'angenommen':
                color = 'green';
                break;
            default:
                color = 'default';
        }
        return <Tag color={color}>{status}</Tag>;
    };

    const columns = [
        {
            title: 'Player',
            dataIndex: 'player_name',
            key: 'player',
            render: (text, record) => (
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src={record.player_image} alt="Player" style={{ maxWidth: '30px', maxHeight: '30px', marginRight: '10px' }} />
                    {text}
                </div>
            ),
        },
        {
            title: 'Mannschaft',
            dataIndex: 'team_name',
            key: 'team',
            render: (text, record) => (
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <img src={record.team_image} alt="Team Logo" style={{ maxWidth: '30px', maxHeight: '30px', marginRight: '10px' }} />
                    {text}
                </div>
            ),
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            render: (status) => getStatusTag(status),
        },
        {
            title: 'Antragsdatum',
            dataIndex: 'created',
            key: 'created',
            render: (created) => moment(created).format('DD/MM/YYYY'),
        }
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Transfer Index</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Transfer Index" style={{ marginTop: 16 }}>
                <Table dataSource={dataSource} columns={columns} loading={loading} rowKey="id" />
            </Card>
        </div>
    );
}
