import React, { useState, useEffect } from 'react';
import { Breadcrumb, Card, Button, Table, Modal, Form, Input, Select } from 'antd'; // Import Select
import { useHistory } from 'react-router-dom';
import { RiCloseFill } from 'react-icons/ri';
import ApiService from "../../../../../network/ApiService";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";

const { Option } = Select; // Destructure Option from Select
const { confirm } = Modal;

export default function AdminNewsIndex() {
    const [news, setNews] = useState([]);
    const [visible, setVisible] = useState(false); // State to control modal visibility
    const [imageUrl, setImageUrl] = useState(''); // State for storing uploaded image URL
    const [leagues, setLeagues] = useState([]); // State for storing leagues
    const history = useHistory();
    const apiService = new ApiService();
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 25,
        total: 0
    });
    const [loading, setLoading] = useState(false);
    const [form] = Form.useForm();

    // Fetch news and leagues when the component mounts
    useEffect(() => {
        fetchNews(pagination.current, pagination.pageSize);
        fetchLeagues();
    }, [pagination.current, pagination.pageSize]);

    const fetchNews = async (page, pageSize) => {
        setLoading(true);
        try {
            const endpoint = `news/all?per=${pageSize}&page=${page}`;
            const response = await apiService.get(endpoint);

            // Check if the response is an array or contains items
            const items = Array.isArray(response) ? response : response.items || [];
            const total = response.metadata ? response.metadata.total : items.length;

            setNews(items);
            setPagination(prevPagination => ({
                ...prevPagination,
                total,
                current: page,
            }));
        } catch (error) {
            console.error('Failed to fetch news:', error);
        } finally {
            setLoading(false);
        }
    };

    const fetchLeagues = async () => {
        try {
            const response = await apiService.get('leagues?per=200');
            const leagueItems = Array.isArray(response.items) ? response.items : [];
            setLeagues(leagueItems); // Set leagues data
        } catch (error) {
            console.error('Failed to fetch leagues:', error);
        }
    };

    const handleTableChange = (pagination) => {
        setPagination({
            ...pagination,
            current: pagination.current,
            pageSize: pagination.pageSize
        });
    };

    const handleUploadSuccess = (url) => {
        setImageUrl(url); // Set the uploaded image URL
    };

    const handleCreateNews = async () => {
        try {
            // Validate the form fields
            const values = await form.validateFields();

            // Prepare the data to be posted
            const postData = {
                text: values.text,    // News text
                image: imageUrl,      // Uploaded image URL
                tag: values.tag,      // News tag (selected league or 'Alle')
                title: values.title,  // News title
            };

            // Simulate the POST request (you can log the data to the console for debugging)
            console.log('Posting news data:', JSON.stringify(postData));

            // Make the POST request to the API
            const response = await apiService.post('news', postData);

            // Check if the response is successful (modify based on how your ApiService handles response statuses)
            if (response) {
                alert('News created successfully!');
                form.resetFields();  // Reset the form fields
                setVisible(false);   // Close the modal
                fetchNews(pagination.current, pagination.pageSize); // Refresh the news list
            } else {
                throw new Error('Failed to create news. Please try again.');
            }
        } catch (errorInfo) {
            console.log('Error occurred:', errorInfo);
            // Show an alert or message to indicate the failure
            alert('An error occurred while creating the news. Please check your inputs and try again.');
        }
    };

    const handleDelete = async (id) => {
        try {
            const response = await apiService.delete(`news/${id}`);

            // Check if the response status indicates success (e.g., 200 or 204)
                alert('News deleted successfully!');
                fetchNews(pagination.current, pagination.pageSize); // Refresh the news list

        } catch (error) {
            console.error('Error deleting news:', error);
            alert('An error occurred while deleting the news.');
        }
    };

    const showDeleteConfirm = (record) => {
        confirm({
            title: 'Sind Sie sicher das Sie diesen Artikel löschen möchten?',
            content: `Artikel: ${record.title}`,
            okText: 'Ja',
            okType: 'danger',
            cancelText: 'Nein',
            onOk() {
                handleDelete(record.id);
            }
        });
    };

    const columns = [
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (image) => image ? <img src={image} alt="news" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        {
            title: 'Titel',
            dataIndex: 'title',
            key: 'title',
            render: (title) => (
                <div style={{
                    maxWidth: '150px',
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    WebkitLineClamp: 2,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    whiteSpace: 'normal'
                }}>
                    {title}
                </div>
            )
        },
        {
            title: 'Text',
            dataIndex: 'text',
            key: 'text',
            render: (text) => (
                <div style={{
                    display: '-webkit-box',
                    WebkitBoxOrient: 'vertical',
                    WebkitLineClamp: 3,
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    maxWidth: '400px',
                    whiteSpace: 'normal'
                }}>
                    {text}
                </div>
            )
        },
        {
            title: 'Tag',
            dataIndex: 'tag',
            key: 'tag'
        },
        {
            title: 'Created',
            dataIndex: 'created',
            key: 'created',
            render: (created) => {
                const date = new Date(created);
                const formattedDate = `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}, ${date.getHours()}:${date.getMinutes().toString().padStart(2, '0')}`;
                return formattedDate;
            }
        },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <>
                    <Button
                        type="text"
                        className="hp-text-color-primary-1 hp-hover-bg-primary-4"
                        onClick={() => {
                            sessionStorage.setItem("newsDetail", JSON.stringify(record));
                            history.push(`/admin/news_detail/${record.id}`);
                        }}
                    >
                        Details
                    </Button>
                    <Button
                        type="danger"
                        onClick={() => showDeleteConfirm(record)}
                    >
                        Löschen
                    </Button>
                </>
            ),
        }
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/admin/news">News</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>News Detail</Breadcrumb.Item>
            </Breadcrumb>

            {/* Button to create new news positioned on the top right */}
            <div style={{ display: 'flex', justifyContent: 'flex-end', marginBottom: 16 }}>
                <Button
                    type="primary"
                    onClick={() => setVisible(true)} // Open modal when clicked
                >
                    Neuen Artikel Erstellen
                </Button>
            </div>

            <Card title="Artikel Übersicht" style={{ marginTop: 16 }}>
                <Table
                    columns={columns}
                    dataSource={news}
                    pagination={pagination}
                    loading={loading}
                    onChange={handleTableChange}
                    rowKey="id"
                />
            </Card>

            {/* Modal for creating news */}
            <Modal
                title="Neuen Artikel Erstellen"
                width={400}
                visible={visible}
                onCancel={() => setVisible(false)}
                footer={[
                    <Button key="cancel" onClick={() => setVisible(false)}>
                        Cancel
                    </Button>,
                    <Button key="submit" type="primary" onClick={handleCreateNews}>
                        Create
                    </Button>,
                ]}
                closeIcon={<RiCloseFill className="remix-icon text-color-black-100" size={24} />}
            >
                <Form form={form} layout="vertical" name="createNewsForm">
                    <Form.Item
                        label="Titel"
                        name="title"
                        rules={[{ required: true, message: 'Please input the title!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Inhalt"
                        name="text"
                        rules={[{ required: true, message: 'Please input the news text!' }]}
                    >
                        <Input.TextArea rows={4} />
                    </Form.Item>
                    {/* Firebase Image Upload Button */}
                    <Form.Item
                        label="Feature Bild"
                    >
                        <FirebaseImageUpload
                            onUploadSuccess={handleUploadSuccess}
                            path={`news/${Date.now()}`}
                            filename="news-image"
                            existingUrl={imageUrl}
                            buttonText="Bild Hochladem"
                        />
                    </Form.Item>
                    {/* Leagues Dropdown with optional 'Alle' */}
                    <Form.Item
                        label="Tag"
                        name="tag"
                    >
                        <Select
                            placeholder="Select a League or Alle"
                            optionLabelProp="children" // Ensure the name (children) is displayed in the dropdown
                        >
                            <Option key="alle" value="Alle">Alle</Option>
                            {leagues.map(league => (
                                <Option key={league.id} value={league.code}>
                                    {league.name}
                                </Option>
                            ))}
                        </Select>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
