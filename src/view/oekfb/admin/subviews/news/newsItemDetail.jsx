import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Breadcrumb, Card, Descriptions, Spin, Input, Button, message, Modal } from 'antd';
import moment from 'moment';
import ApiService from "../../../../../network/ApiService";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";
import { useHistory } from "react-router-dom";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css'; // Import Quill styles

export default function NewsItemDetail() {
    const { id } = useParams();
    const [newsItem, setNewsItem] = useState(null);
    const [originalNewsItem, setOriginalNewsItem] = useState(null);
    const [loading, setLoading] = useState(true);
    const [isDeleteModalVisible, setDeleteModalVisible] = useState(false);
    const history = useHistory();

    useEffect(() => {
        const fetchNewsItem = async () => {
            setLoading(true);
            try {
                const newsResponse = await new ApiService().get(`news/${id}`);
                setNewsItem(newsResponse);
                setOriginalNewsItem(newsResponse);
            } catch (error) {
                console.error('Failed to fetch news details:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchNewsItem();
    }, [id]);

    const handleInputChange = (field, value) => {
        setNewsItem({ ...newsItem, [field]: value });
    };

    const hasNewsItemChanged = () => {
        return JSON.stringify(newsItem) !== JSON.stringify(originalNewsItem);
    };

    const handleUpdateNewsItem = async () => {
        if (hasNewsItemChanged()) {
            const apiService = new ApiService();
            try {
                await apiService.patch(`news/${newsItem.id}`, newsItem);
                sessionStorage.setItem("newsItemDetail", JSON.stringify(newsItem));
                setOriginalNewsItem(newsItem);
                message.success('News erfolgreich aktualisiert');
            } catch (error) {
                message.error('Fehler beim Aktualisieren der News');
                console.error('Update news error:', error);
            }
        } else {
            message.info('Keine Änderungen zu aktualisieren');
        }
    };

    const handleUpload = async (url) => {
        try {
            const updatedNewsItem = {
                ...newsItem,
                image: url,
            };
            setNewsItem(updatedNewsItem);
            await new ApiService().patch(`news/${newsItem.id}`, updatedNewsItem);
            setOriginalNewsItem(updatedNewsItem);
            message.success('Bild hochgeladen und News erfolgreich aktualisiert');
        } catch (error) {
            console.error(`Fehler beim Aktualisieren der News mit hochgeladenem Bild`, error);
            message.error(`Fehler beim Aktualisieren der News mit hochgeladenem Bild`);
        }
    };

    const handleDeleteNewsItem = async () => {
        try {
            await new ApiService().delete(`news/${newsItem.id}`);
            message.success("News erfolgreich gelöscht");
            setDeleteModalVisible(false);
            history.push('/admin/news');
        } catch (error) {
            message.error("Fehler beim Löschen der News");
            console.error("Delete news error:", error);
        }
    };

    const confirmDeleteNewsItem = () => {
        Modal.confirm({
            title: 'Sind Sie sicher, dass Sie diese News löschen möchten?',
            content: 'Diese Aktion kann nicht rückgängig gemacht werden.',
            onOk: handleDeleteNewsItem,
            okText: 'Löschen',
            cancelText: 'Abbrechen',
            okButtonProps: { danger: true },
        });
    };

    if (loading) {
        return <Spin size="large" />;
    }

    if (!newsItem) {
        return <p>News nicht gefunden</p>;
    }

    return (
        <div style={{ padding: 24 }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/admin/news">News Übersicht</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>News Detail</Breadcrumb.Item>
                </Breadcrumb>
                <div>
                    <Button
                        type="primary"
                        onClick={handleUpdateNewsItem}
                        disabled={!hasNewsItemChanged()}
                        style={{ marginRight: 10 }}
                    >
                        Update
                    </Button>
                    <Button
                        type="danger"
                        onClick={confirmDeleteNewsItem}
                    >
                        News Löschen
                    </Button>
                </div>
            </div>

            <div style={{ marginTop: 16 }}>
                <Card title="News Information">
                    <div style={{ width: '100%', maxWidth: "200px", marginBottom: "20px" }}>
                        <img src={newsItem.image} alt="News" style={{ width: '100%', borderRadius: '10px' }} />
                        <div style={{ marginTop: "20px" }}>
                            <FirebaseImageUpload
                                onUploadSuccess={(url) => handleUpload(url)}
                                path={`news/${newsItem.id}`}
                                filename="news_image"
                                buttonText={"News Bild Hochladen"}
                            />
                        </div>
                    </div>

                    <Descriptions bordered column={1}>
                        <Descriptions.Item label="Tag">
                            <Input
                                value={newsItem.tag}
                                onChange={(e) => handleInputChange('tag', e.target.value)}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="ID/UUID">{newsItem.id}</Descriptions.Item>
                        <Descriptions.Item label="Titel">
                            <Input
                                value={newsItem.title}
                                onChange={(e) => handleInputChange('title', e.target.value)}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="Text">
                            <ReactQuill
                                value={newsItem.text}
                                onChange={(value) => handleInputChange('text', value)}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="Erstellt am">
                            {moment(newsItem.created).format("YYYY-MM-DD HH:mm")}
                        </Descriptions.Item>
                    </Descriptions>
                </Card>
            </div>
        </div>
    );
}
