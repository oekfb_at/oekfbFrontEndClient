import React, { useState, useEffect } from 'react';
import { Card, Descriptions, Button, message, Breadcrumb, Switch } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from "../../../../../network/ApiService";
import moment from 'moment'; // to format the created date

/**
 * Component for displaying and managing strafsenat details.
 * @returns JSX element
 */
const StrafsenatDetail = () => {
    const [strafsenatData, setStrafsenatData] = useState(null);
    const [referee, setReferee] = useState(null); // For storing referee data
    const [match, setMatch] = useState(null); // For storing match data
    const api = new ApiService();
    const history = useHistory();

    useEffect(() => {
        fetchStrafsenatData();
    }, []);

    // Fetches strafsenat data from session storage and API
    const fetchStrafsenatData = async () => {
        try {
            const storedStrafsenat = JSON.parse(sessionStorage.getItem("strafsenatDetail"));
            if (storedStrafsenat && storedStrafsenat.id) {
                const response = await api.get(`strafsenat/${storedStrafsenat.id}`);
                setStrafsenatData(response);

                // Fetch referee and filter the assignments to find the match by match ID
                fetchRefereeData(response.ref_id, response.match_id);
            }
        } catch (error) {
            console.error('Fehler beim Abrufen der Strafsenat-Daten:', error);
            message.error('Fehler beim Abrufen der Strafsenat-Daten');
        }
    };

    // Fetches referee data by referee ID and filters assignments to find the match by match ID
    const fetchRefereeData = async (refereeId, matchId) => {
        try {
            const response = await api.get(`referees/${refereeId}`);
            setReferee(response);

            // Filter assignments to find the match with the given match ID
            const assignedMatch = response.assignments.find((assignment) => assignment.id === matchId);
            if (assignedMatch) {
                setMatch(assignedMatch);
            } else {
                message.error('Spiel für den Schiedsrichter nicht gefunden.');
            }
        } catch (error) {
            console.error('Fehler beim Abrufen der Schiedsrichter-Daten oder des Spiels:', error);
            message.error('Fehler beim Abrufen der Schiedsrichter-Daten oder des Spiels.');
        }
    };

    // Handles toggling the strafsenat status
    const handleStatusToggle = async (checked) => {
        try {
            const url = `strafsenat/${strafsenatData.id}/${checked ? 'open' : 'close'}`;
            await api.patch(url);
            message.success(`Strafsenat erfolgreich ${checked ? 'geöffnet' : 'geschlossen'}`);
            setStrafsenatData({ ...strafsenatData, offen: checked });
        } catch (error) {
            console.error(`Fehler beim ${checked ? 'Öffnen' : 'Schließen'} des Strafsenats:`, error);
            message.error(`Fehler beim ${checked ? 'Öffnen' : 'Schließen'} des Strafsenats`);
        }
    };

    // Handles the deletion of the strafsenat
    const handleDeleteStrafsenat = async () => {
        try {
            if (!strafsenatData || !strafsenatData.id) {
                throw new Error('Strafsenat-ID nicht verfügbar');
            }

            await api.delete(`strafsenat/${strafsenatData.id}`);
            message.success('Strafsenat erfolgreich gelöscht');
            history.push('/admin/referee'); // Navigate back to strafsenat list
        } catch (error) {
            console.error('Fehler beim Löschen des Strafsenats:', error);
            message.error('Fehler beim Löschen des Strafsenats');
        }
    };

    if (!strafsenatData || !referee || !match) {
        return <p>Laden...</p>;
    }

    return (
        <div style={{ display: 'flex', flexDirection: 'column', gap: '16px' }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/admin/strafsenat">Strafsenat</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Strafsenat Detail</Breadcrumb.Item>
                </Breadcrumb>

                {/* Switch to toggle the status of Strafsenat */}
                <div>
                    <label style={{ marginRight: '8px' }}>Status: </label>
                    <Switch
                        checked={strafsenatData.offen}
                        onChange={handleStatusToggle}
                        checkedChildren="Offen"
                        unCheckedChildren="Geschlossen"
                    />
                </div>
            </div>

            <Card title="Strafsenat Informationen">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Strafsenat ID">{strafsenatData.id}</Descriptions.Item>
                    <Descriptions.Item label="Spiel ID">{strafsenatData.match_id}</Descriptions.Item>
                    <Descriptions.Item label="Erstellt">
                        {moment(strafsenatData.created).format('DD.MM.YYYY HH:mm')}
                    </Descriptions.Item>
                    <Descriptions.Item label="Status">
                        {strafsenatData.offen ? 'Offen' : 'Geschlossen'}
                    </Descriptions.Item>
                    <Descriptions.Item label="Schiedsrichter ID">{strafsenatData.ref_id}</Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Spielbericht">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Schiedsrichter Spielbericht">
                        <pre style={{ whiteSpace: 'pre-wrap', wordWrap: 'break-word' }}>{strafsenatData.text}</pre>
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Schiedsrichter Informationen">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Schiedsrichter Name">{referee.name}</Descriptions.Item>
                    <Descriptions.Item label="Schiedsrichter Nationalität">{referee.nationality}</Descriptions.Item>
                    <Descriptions.Item label="Schiedsrichter Bild">
                        <img src={referee.image} alt="Schiedsrichter" style={{ width: 100, height: 100 }} />
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Spiel Informationen">
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Heimteam">
                        <img src={match.home_blanket.logo} alt="Heimteam Logo" style={{ width: 50, height: 50, marginRight: 10 }} />
                        {match.home_blanket.name}
                    </Descriptions.Item>
                    <Descriptions.Item label="Auswärtsteam">
                        <img src={match.away_blanket.logo} alt="Auswärtsteam Logo" style={{ width: 50, height: 50, marginRight: 10 }} />
                        {match.away_blanket.name}
                    </Descriptions.Item>
                    <Descriptions.Item label="Ergebnis">
                        {match.score.home} - {match.score.away}
                    </Descriptions.Item>
                    <Descriptions.Item label="Stadion">{match.details.stadium}</Descriptions.Item>
                    <Descriptions.Item label="Ort">{match.details.location}</Descriptions.Item>
                    <Descriptions.Item label="Datum">
                        {moment(match.details.date).format('DD.MM.YYYY HH:mm')}
                    </Descriptions.Item>
                </Descriptions>
            </Card>
        </div>
    );
};

export default StrafsenatDetail;
