import React, { useState, useEffect } from 'react';
import { Breadcrumb, Card, Button, Table, Tag } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from "../../../../../network/ApiService";
import moment from 'moment'; // to format dates

export default function StrafsenatIndex() {
    const [strafsenat, setStrafsenat] = useState([]);
    const history = useHistory();
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 50,
        total: 0
    });
    const [loading, setLoading] = useState(false);
    const [sorter, setSorter] = useState({ order: 'descend', field: 'created' });

    useEffect(() => {
        fetchSenat(pagination.current, pagination.pageSize, sorter);
    }, [pagination.current, pagination.pageSize, sorter]);

    const fetchSenat = async (page, pageSize, sorter) => {
        setLoading(true);
        try {
            const endpoint = `strafsenat?per=${pageSize}&page=${page}&sort=${sorter.field}&order=${sorter.order}`;
            const response = await new ApiService().get(endpoint);

            const items = Array.isArray(response) ? response : response.items || [];
            const metadata = response.metadata || { total: items.length, page };

            setStrafsenat(items);
            setPagination(prevPagination => ({
                ...prevPagination,
                total: metadata.total,
                current: metadata.page,
            }));
        } catch (error) {
            console.error('Failed to fetch referees:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleTableChange = (pagination, filters, sorter) => {
        setPagination({
            ...pagination,
            current: pagination.current,
            pageSize: pagination.pageSize
        });

        if (sorter.field && sorter.order) {
            setSorter({
                field: sorter.field,
                order: sorter.order,
            });
        }
    };

    const columns = [
        {
            title: 'Erstellungsdatum',
            dataIndex: 'created',
            key: 'created',
            sorter: true, // Allow sorting by this column
            defaultSortOrder: 'descend', // Default sort by newest
            render: (created) => moment(created).format('DD.MM.YYYY HH:mm'), // Format date
        },
        {
            title: 'Status',
            dataIndex: 'offen',
            key: 'offen',
            filters: [
                { text: 'Offen', value: true },
                { text: 'Geschlossen', value: false },
            ],
            onFilter: (value, record) => record.offen === value,
            render: (offen) => (
                <Tag color={offen ? 'orange' : 'green'}>
                    {offen ? 'Offen' : 'Geschlossen'}
                </Tag>
            ),
        },
        {
            title: 'Match ID',
            dataIndex: 'match_id',
            key: 'match_id',
        },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button
                    type="text"
                    className="hp-text-color-primary-1 hp-hover-bg-primary-4"
                    onClick={() => {
                        sessionStorage.setItem("strafsenatDetail", JSON.stringify(record));
                        history.push(`/admin/strafsenat/detail/${record.id}`);
                    }}
                >
                    Details
                </Button>
            ),
        }
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/admin/referee">Strafsenat</a>
                </Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Strafsenat Index" style={{ marginTop: 16 }}>
                <Table
                    columns={columns}
                    dataSource={strafsenat}
                    pagination={pagination}
                    loading={loading}
                    onChange={handleTableChange}
                    rowKey="id"
                />
            </Card>
        </div>
    );
}
