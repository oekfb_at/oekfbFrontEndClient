import React, { useState, useEffect } from 'react';
import { Breadcrumb, Card, Button, Table } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from "../../../../../network/ApiService";

export default function AdminRefereeIndex() {
    const [referees, setReferees] = useState([]);
    const history = useHistory();
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 50,
        total: 0
    });
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        fetchReferees(pagination.current, pagination.pageSize);
    }, [pagination.current, pagination.pageSize]);

    const fetchReferees = async (page, pageSize) => {
        setLoading(true);
        try {
            const endpoint = `referees?per=${pageSize}&page=${page}`;
            const response = await new ApiService().get(endpoint);

            const items = Array.isArray(response) ? response : response.items || [];
            const metadata = response.metadata || { total: items.length, page };

            setReferees(items);
            setPagination(prevPagination => ({
                ...prevPagination,
                total: metadata.total,
                current: metadata.page,
            }));
        } catch (error) {
            console.error('Failed to fetch referees:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleTableChange = (pagination) => {
        setPagination({
            ...pagination,
            current: pagination.current,
            pageSize: pagination.pageSize
        });
    };

    const columns = [
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (image) => image ? <img src={image} alt="referee" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        { title: 'User Name', dataIndex: "name", key: 'name' },
        { title: 'User ID', dataIndex: ['user', 'id'], key: 'userId' },
        { title: 'Balance', dataIndex: 'balance', key: 'balance' },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button
                    type="text"
                    className="hp-text-color-primary-1 hp-hover-bg-primary-4"
                    onClick={() => {
                        sessionStorage.setItem("refereeDetail", JSON.stringify(record));
                        history.push(`/admin/referee_detail/${record.id}`);
                    }}
                >
                    Details
                </Button>
            ),
        }
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/admin/referee">Schiedrichter</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Schiedrichter Detail</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Schiedrichter Index" style={{ marginTop: 16 }}>
                <Table
                    columns={columns}
                    dataSource={referees}
                    pagination={pagination}
                    loading={loading}
                    onChange={handleTableChange}
                    rowKey="id"
                />
            </Card>
        </div>
    );
}
