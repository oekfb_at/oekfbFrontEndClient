import React, { useState, useEffect, useMemo } from 'react';
import { Breadcrumb, Card, Descriptions, Spin, Input, Button, Select, message, Modal, InputNumber, Table } from 'antd';
import ApiService from "../../../../../network/ApiService";
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";
import { useHistory } from 'react-router-dom';

const { Option } = Select;

export default function RefereeDetail() {
    const [referee, setReferee] = useState(null);
    const [originalReferee, setOriginalReferee] = useState(null);
    const [loading, setLoading] = useState(true);
    const [isTopUpModalVisible, setTopUpModalVisible] = useState(false);
    const [topUpAmount, setTopUpAmount] = useState(0);
    const [teams, setTeams] = useState({});
    const [user, setUser] = useState({});
    const [stadiums, setStadiums] = useState([]);
    const [isDeleteModalVisible, setDeleteModalVisible] = useState(false);
    const history = useHistory();

    const countries = [
        "Afghanistan", "Albanien", "Algerien", "Andorra", "Angola", "Antigua und Barbuda", "Argentinien",
        "Armenien", "Australien", "Österreich", "Aserbaidschan", "Bahamas", "Bahrain", "Bangladesch", "Barbados",
        "Weißrussland", "Belgien", "Belize", "Benin", "Bhutan", "Bolivien", "Bosnien und Herzegowina", "Botswana",
        "Brasilien", "Brunei", "Bulgarien", "Burkina Faso", "Burundi", "Kap Verde", "Kambodscha", "Kamerun",
        "Kanada", "Zentralafrikanische Republik", "Tschad", "Chile", "China", "Kolumbien", "Komoren", "Kongo",
        "Costa Rica", "Kroatien", "Kuba", "Zypern", "Tschechische Republik", "Dänemark", "Dschibuti", "Dominica",
        "Dominikanische Republik", "Ecuador", "Ägypten", "El Salvador", "Äquatorialguinea", "Eritrea", "Estland",
        "Eswatini", "Äthiopien", "Fidschi", "Finnland", "Frankreich", "Gabun", "Gambia", "Georgien", "Deutschland",
        "Ghana", "Griechenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras",
        "Ungarn", "Island", "Indien", "Indonesien", "Iran", "Irak", "Irland", "Israel", "Italien", "Jamaika",
        "Japan", "Jordanien", "Kasachstan", "Kenia", "Kiribati", "Kuwait", "Kirgistan", "Laos", "Lettland",
        "Libanon", "Lesotho", "Liberia", "Libyen", "Liechtenstein", "Litauen", "Luxemburg", "Madagaskar",
        "Malawi", "Malaysia", "Maldiven", "Mali", "Malta", "Marshallinseln", "Mauretanien", "Mauritius",
        "Mexiko", "Mikronesien", "Moldau", "Monaco", "Mongolei", "Montenegro", "Marokko", "Mosambik",
        "Myanmar", "Namibia", "Nauru", "Nepal", "Niederlande", "Neuseeland", "Nicaragua", "Niger",
        "Nigeria", "Nordkorea", "Nordmazedonien", "Norwegen", "Oman", "Pakistan", "Palau", "Palästina",
        "Panama", "Papua-Neuguinea", "Paraguay", "Peru", "Philippinen", "Polen", "Portugal", "Katar",
        "Rumänien", "Russland", "Ruanda", "St. Kitts und Nevis", "St. Lucia", "St. Vincent und die Grenadinen",
        "Samoa", "San Marino", "Sao Tome und Principe", "Saudi-Arabien", "Senegal", "Serbien", "Seychellen",
        "Sierra Leone", "Singapur", "Slowakei", "Slowenien", "Salomonen", "Somalia", "Südafrika",
        "Südkorea", "Südsudan", "Spanien", "Sri Lanka", "Sudan", "Surinam", "Schweden", "Schweiz",
        "Syrien", "Taiwan", "Tadschikistan", "Tansania", "Thailand", "Timor-Leste", "Togo", "Tonga", "Trinidad und Tobago",
        "Tunesien", "Türkei", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "Vereinigte Arabische Emirate",
        "Vereinigtes Königreich", "Vereinigte Staaten", "Uruguay", "Usbekistan", "Vanuatu", "Vatikanstadt", "Venezuela",
        "Vietnam", "Jemen", "Sambia", "Simbabwe"
    ];

    useEffect(() => {
        const fetchRefereeAndStadiums = async () => {
            setLoading(true);
            try {
                const storedRef = JSON.parse(sessionStorage.getItem("refereeDetail"));

                if (storedRef && storedRef.id) {
                    const refResponse = await new ApiService().get(`referees/${storedRef.id}`);
                    setReferee(refResponse);
                    setOriginalReferee(refResponse);

                    if (refResponse.user.id) {
                        const userResponse = await new ApiService().get(`users/${refResponse.user.id}`);
                        setUser(userResponse);
                    }

                    const stadiumsResponse = await new ApiService().get('stadiums');
                    setStadiums(stadiumsResponse.items);
                } else {
                    console.error('No referee ID found in session storage');
                }
            } catch (error) {
                console.error('Failed to fetch referee details:', error);
            } finally {
                setLoading(false);
            }
        };
        fetchRefereeAndStadiums();
    }, []);

    const handleInputChange = (field, value) => {
        setReferee({ ...referee, [field]: value });
    };

    const hasRefereeChanged = () => {
        return JSON.stringify(referee) !== JSON.stringify(originalReferee);
    };

    const showTopUpModal = () => {
        setTopUpModalVisible(true);
    };

    const handleTopUpModalCancel = () => {
        setTopUpModalVisible(false);
        setTopUpAmount(0);
    };

    const handleTopUp = async () => {
        if (topUpAmount <= 0) {
            message.error('Amount must be positive');
            return;
        }

        try {
            await new ApiService().get(`referees/${referee.id}/topup/${topUpAmount}`);
            setReferee({ ...referee, balance: (referee.balance || 0) + topUpAmount });
            setTopUpAmount(0);
            setTopUpModalVisible(false);
            message.success('Balance updated successfully');
        } catch (error) {
            message.error('Failed to update balance');
            console.error('Top up balance error:', error);
        }
    };

    const handleRefereeUpdate = async () => {
        if (hasRefereeChanged()) {
            const apiService = new ApiService();
            try {
                const { assignments, ...refereeWithoutAssignments } = referee;

                await apiService.patch(`referees/${referee.id}`, refereeWithoutAssignments);

                sessionStorage.setItem("refereeDetail", JSON.stringify(referee));
                setOriginalReferee(referee);
                message.success('Referee updated successfully');
            } catch (error) {
                message.error('Failed to update referee');
                console.error('Update referee error:', error);
            }
        } else {
            message.info('No changes to update');
        }
    };

    const handleUpload = async (url, field) => {
        try {
            const updatedReferee = {
                ...referee,
                [field]: url,
            };
            setReferee(updatedReferee);
            await new ApiService().patch(`referees/${referee.id}`, updatedReferee);
            setOriginalReferee(updatedReferee);
            message.success('File uploaded and referee updated successfully');
        } catch (error) {
            console.error(`Failed to update referee with uploaded file: ${field}`, error);
            message.error(`Failed to update referee with uploaded file: ${field}`);
        }
    };

    const getTeamById = async (id) => {
        if (teams[id]) {
            return teams[id];
        }

        try {
            const teamResponse = await new ApiService().get(`teams/${id}`);
            setTeams(prevTeams => ({
                ...prevTeams,
                [id]: teamResponse,
            }));
            return teamResponse;
        } catch (error) {
            console.error(`Failed to fetch team with ID: ${id}`, error);
            return null;
        }
    };

    const showDeleteModal = () => {
        setDeleteModalVisible(true);
    };

    const handleDeleteModalCancel = () => {
        setDeleteModalVisible(false);
    };

    const handleRefereeDelete = async () => {
        try {
            if (referee.assignments && referee.assignments.length > 0) {
                message.error("Sie können Schiedrichter nicht Löschen solange sie Zugewiesene Spiele haben.");
                return;
            }
            await new ApiService().delete(`referees/${referee.id}`);
            message.success("Referee deleted successfully");
            setDeleteModalVisible(false);
            history.push('/admin/referee');
            // Optionally refresh the page or redirect to referee list
        } catch (error) {
            message.error("Failed to delete referee");
            console.error("Delete referee error:", error);
        }
    };

    const matchColumns = [
        {
            title: 'Home',
            dataIndex: ['home_team', 'id'],
            key: 'home_team',
            render: (id) => {
                const [homeTeam, setHomeTeam] = useState(null);

                useEffect(() => {
                    const fetchHomeTeam = async () => {
                        const team = await getTeamById(id);
                        setHomeTeam(team);
                    };
                    fetchHomeTeam();
                }, [id]);

                if (!homeTeam) {
                    return <Spin size="small" />;
                }

                return (
                    <div style={{ textAlign: 'center' }}>
                        {homeTeam?.logo && (
                            <img
                                src={homeTeam.logo}
                                alt="Home Team Logo"
                                style={{ maxWidth: '30px', maxHeight: '30px', marginBottom: '4px' }}
                            />
                        )}
                        <div>{homeTeam?.team_name || 'Unknown'}</div>
                    </div>
                );
            },
        },
        {
            title: 'Away',
            dataIndex: ['away_team', 'id'],
            key: 'away_team',
            render: (id) => {
                const [awayTeam, setAwayTeam] = useState(null);

                useEffect(() => {
                    const fetchAwayTeam = async () => {
                        const team = await getTeamById(id);
                        setAwayTeam(team);
                    };
                    fetchAwayTeam();
                }, [id]);

                if (!awayTeam) {
                    return <Spin size="small" />;
                }

                return (
                    <div style={{ textAlign: 'center' }}>
                        {awayTeam?.logo && (
                            <img
                                src={awayTeam.logo}
                                alt="Away Team Logo"
                                style={{ maxWidth: '30px', maxHeight: '30px', marginBottom: '4px' }}
                            />
                        )}
                        <div>{awayTeam?.team_name || 'Unknown'}</div>
                    </div>
                );
            },
        },
        { title: 'Score', dataIndex: 'score', key: 'score', render: (score) => `${score.home} - ${score.away}` },
        { title: 'Status', dataIndex: 'status', key: 'status' },
        { title: 'Stadium', dataIndex: ['details', 'location'], key: 'location' },
        { title: 'Spieltag', dataIndex: ['details', 'gameday'], key: 'gameday' },
        {
            title: 'Date',
            dataIndex: ['details', 'date'],
            key: 'date',
            render: (date) => {
                if (!date) return 'Unknown';
                const parsedDate = new Date(date);
                const day = String(parsedDate.getUTCDate()).padStart(2, '0');
                const month = String(parsedDate.getUTCMonth() + 1).padStart(2, '0');
                const year = parsedDate.getUTCFullYear();
                const hours = String(parsedDate.getUTCHours()).padStart(2, '0');
                const minutes = String(parsedDate.getUTCMinutes()).padStart(2, '0');
                return `${day}.${month}.${year} ${hours}:${minutes}`;
            },
        }
    ];

    const sortedAssignments = useMemo(() => {
        return [...(referee?.assignments || [])].sort((a, b) => {
            const gameDayA = a.details?.gameday || 0;
            const gameDayB = b.details?.gameday || 0;

            if (gameDayA !== gameDayB) {
                return gameDayA - gameDayB; // Sort by gameday
            }

            const dateA = new Date(a.details?.date || 0);
            const dateB = new Date(b.details?.date || 0);

            return dateA - dateB; // Then sort by date
        });
    }, [referee?.assignments]);

    if (loading) {
        return <Spin size="large" />;
    }

    if (!referee) {
        return <p>Referee not found</p>;
    }

    return (
        <div style={{ padding: 24 }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/admin/referees">Schiedrichter Übersicht</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Schiedrichter Detail</Breadcrumb.Item>
                </Breadcrumb>
                <div>
                    <Button
                        type="primary"
                        onClick={handleRefereeUpdate}
                        disabled={!hasRefereeChanged()}
                        style={{ marginRight: 10 }}
                    >
                        Update
                    </Button>
                    <Button
                        type="danger"
                        onClick={showDeleteModal}
                    >
                        Delete Referee
                    </Button>
                </div>
            </div>

            {/*INFO*/}
            <div style={{ marginTop: 16 }}>
                <Card title="Schiedrichter Information">
                    <div style={{ width: '100%', maxWidth: "200px", marginBottom: "20px" }}>
                        <img src={referee.image} alt="Referee" style={{ width: '100%', borderRadius: '10px' }} />
                        <div style={{ marginTop: "20px" }}>
                            <FirebaseImageUpload
                                onUploadSuccess={(url) => handleUpload(url, 'image')}
                                path={`referees/${referee.id}`}
                                filename="image"
                                buttonText={"Schiedrichter Foto Hochladen"}
                            />
                        </div>
                    </div>

                    <Descriptions bordered column={1}>
                        <Descriptions.Item label="Name">
                            <Input
                                value={referee.name}
                                onChange={(e) => handleInputChange('name', e.target.value)}
                            />
                        </Descriptions.Item>
                        <Descriptions.Item label="ID/UUID">{referee.id}</Descriptions.Item>
                        <Descriptions.Item label="Nationalität">
                            <Select
                                value={referee.nationality}
                                onChange={(value) => handleInputChange('nationality', value)}
                                style={{ width: '100%' }}
                            >
                                {countries.map(country => (
                                    <Option key={country} value={country}>
                                        {country}
                                    </Option>
                                ))}
                            </Select>
                        </Descriptions.Item>
                        <Descriptions.Item label="Guthaben">
                            {referee.balance || 0}€
                            <Button type="primary" onClick={showTopUpModal} style={{ marginLeft: 10 }}>
                                Guthaben aufladen
                            </Button>
                        </Descriptions.Item>
                    </Descriptions>
                </Card>
            </div>

            {/*USER*/}
            <div style={{ marginTop: 16 }}>
                <Card title="Kontakt Information">
                    <Descriptions bordered column={1}>
                        <Descriptions.Item label="ID/UUID">{user.id}</Descriptions.Item>
                        <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                    </Descriptions>
                </Card>
            </div>

            {/* MATCHES */}
            <div style={{ marginTop: 16 }}>
                <Card title={`${sortedAssignments.length} Matches Zugeördnet`}>
                    <Table
                        columns={matchColumns}
                        dataSource={sortedAssignments}
                        rowKey="id"
                        pagination={false}
                    />
                </Card>
            </div>

            <Modal
                title="Guthaben aufladen"
                visible={isTopUpModalVisible}
                onOk={handleTopUp}
                onCancel={handleTopUpModalCancel}
            >
                <InputNumber
                    min={0}
                    value={topUpAmount}
                    onChange={setTopUpAmount}
                    style={{ width: '100%' }}
                />
            </Modal>

            <Modal
                title="Confirm Delete Referee"
                visible={isDeleteModalVisible}
                onOk={handleRefereeDelete}
                onCancel={handleDeleteModalCancel}
            >
                <p>Bist du sicher, dass du diesen Schiedsrichter löschen möchtest?</p>
                <p>Diese Aktion kann nicht rückgängig gemacht werden.</p>
            </Modal>
        </div>
    );
}
