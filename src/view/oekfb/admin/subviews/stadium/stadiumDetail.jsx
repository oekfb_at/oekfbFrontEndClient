import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Form, Input, Button, message, Select, Switch, DatePicker } from 'antd';
import { useHistory, useParams } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";
import moment from 'moment';

export default function StadiumDetail() {
    const [stadium, setStadium] = useState(null);
    const [loading, setLoading] = useState(true);
    const [form] = Form.useForm();
    const history = useHistory();
    const { id: paramId } = useParams(); // Get the id from the URL

    useEffect(() => {
        const fetchStadiumDetail = async () => {
            const storedStadium = sessionStorage.getItem("stadiumDetail");

            let stadiumId;
            if (storedStadium) {
                const parsedStadium = JSON.parse(storedStadium);
                stadiumId = parsedStadium.id || paramId; // Use the stored stadium id or fallback to paramId
            } else {
                stadiumId = paramId; // Use the paramId if no stadium is stored
            }

            if (stadiumId) {
                const apiService = new ApiService();
                try {
                    const data = await apiService.get(`stadiums/${stadiumId}`);
                    setStadium(data);
                    sessionStorage.setItem("stadiumDetail", JSON.stringify(data)); // Save the latest data to sessionStorage
                } catch (error) {
                    message.error('Failed to fetch stadium details');
                    console.error('Fetch stadium details error:', error);
                } finally {
                    setLoading(false);
                }
            } else {
                message.error('No stadium ID found');
                setLoading(false);
            }
        };

        fetchStadiumDetail();
    }, [paramId]);

    const onFinish = async (values) => {
        // Ensure that we keep all existing stadium data and merge with updated values
        const formattedValues = {
            ...stadium, // Spread the current stadium state
            ...values,  // Spread the form values, which will overwrite the existing fields if changed
            partnerSince: values.partnerSince ? values.partnerSince.format('YYYY/MM') : undefined,
        };

        const apiService = new ApiService();
        try {
            await apiService.patch(`stadiums/${paramId}`, formattedValues);
            message.success('Stadium details updated successfully');
            history.push('/admin/stadium');
        } catch (error) {
            message.error('Failed to update stadium details');
            console.error('Update stadium details error:', error);
        }
    };

    // Handle image upload success and update stadium state
    const handleUploadSuccess = async (url) => {
        try {
            const updatedStadium = {
                ...stadium, // Spread the existing stadium object to keep all other fields
                image: url, // Update the image field
            };
            setStadium(updatedStadium); // Update the state with the new stadium object

            // Persist the update to the backend
            const apiService = new ApiService();
            await apiService.patch(`stadiums/${paramId}`, updatedStadium); // Send the entire stadium object

            message.success('Image uploaded and stadium updated successfully');
        } catch (error) {
            console.error('Failed to update stadium image', error);
            message.error('Failed to update stadium image');
        }
    };

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/admin/stadiums">Stadions</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Stadiums Detail</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Stadium Detail" style={{ marginTop: 16 }}>
                {loading ? (
                    <p>Loading...</p>
                ) : (
                    <Form
                        form={form}
                        initialValues={{
                            ...stadium,
                            partnerSince: stadium?.partnerSince ? moment(stadium.partnerSince, 'YYYY/MM') : null,
                        }}
                        onFinish={onFinish}
                        layout="vertical"
                    >
                        {/* Image Upload */}
                        <Form.Item label="Stadium Image">
                            <div style={{ marginBottom: "20px" }}>
                                {stadium?.image && (
                                    <img
                                        src={stadium.image}
                                        alt="Stadium"
                                        style={{ width: '100%', maxWidth: "200px", marginBottom: "10px", borderRadius: '10px' }}
                                    />
                                )}
                                <FirebaseImageUpload
                                    onUploadSuccess={handleUploadSuccess}
                                    path={`stadiums/${paramId}`}
                                    filename="stadium_image"
                                    buttonText="Stadium Image Hochladen"
                                />
                            </div>
                        </Form.Item>

                        {/* Other form fields for stadium details */}
                        <Form.Item name="code" label="Stadium Code (Bsp: LSF3)" rules={[{ required: true, message: 'Bitte geben Sie den Code ein!' }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="name" label="Name" rules={[{ required: true, message: 'Bitte geben Sie den Namen ein!' }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="address" label="Address" rules={[{ required: true, message: 'Bitte geben Sie die Adresse ein!' }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="type" label="Type (Bsp: Kunstrasen)" rules={[{ required: true, message: 'Bitte geben Sie den Typ ein!' }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="schuhwerk" label="Schuhwerk (Bsp: Kunstrasen)" rules={[{ required: true, message: 'Bitte geben Sie das Schuhwerk ein!' }]}>
                            <Input />
                        </Form.Item>
                        <Form.Item name="parking" label="Gibt es Parkplätze?" valuePropName="checked">
                            <Switch defaultChecked={stadium?.parking} />
                        </Form.Item>
                        <Form.Item name="flutlicht" label="Gibt es Flutlicht?" valuePropName="checked">
                            <Switch defaultChecked={stadium?.flutlicht} />
                        </Form.Item>
                        <Form.Item name="partnerSince" label="Partner der ÖKFB seit ">
                            <DatePicker format="YYYY/MM" picker="month" />
                        </Form.Item>
                        <Form.Item name="homeTeam" label="Heim Verein des Stadiums">
                            <Input />
                        </Form.Item>
                        <Form.Item name="bundesland" label="Bundesland" rules={[{ required: true, message: 'Bitte wählen Sie ein Bundesland aus!' }]}>
                            <Select>
                                <Select.Option value="wien">Wien</Select.Option>
                                <Select.Option value="niederoesterreich">Niederösterreich</Select.Option>
                                <Select.Option value="oberoesterreich">Oberösterreich</Select.Option>
                                <Select.Option value="steiermark">Steiermark</Select.Option>
                                <Select.Option value="kaernten">Kärnten</Select.Option>
                                <Select.Option value="salzburg">Salzburg</Select.Option>
                                <Select.Option value="tirol">Tirol</Select.Option>
                                <Select.Option value="vorarlberg">Vorarlberg</Select.Option>
                                <Select.Option value="burgenland">Burgenland</Select.Option>
                            </Select>
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit">
                                Aktualisieren
                            </Button>
                        </Form.Item>
                    </Form>
                )}
            </Card>
        </div>
    );
}
