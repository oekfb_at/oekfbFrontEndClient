import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Table, message, Button } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';

/**
 * StadiumIndex component fetches and displays a list of stadiums.
 * Users can navigate to the StadiumDetail component by clicking on the Details button.
 *
 * @component
 */
export default function StadiumIndex() {
    const [dataSource, setDataSource] = useState([]);
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    useEffect(() => {
        /**
         * Fetches the stadium data from the API and sets it in the state.
         */
        const fetchData = async () => {
            const apiService = new ApiService();
            try {
                const data = await apiService.get('stadiums?per=1000');
                if (data && data.items) {
                    const formattedData = data.items.map((item, index) => ({
                        key: index,
                        id: item.id,
                        name: item.name,
                        bundesland: item.bundesland,
                        partner_since: item.partner_since,
                        parking: item.parking,
                        home_team: item.home_team,
                        schuhwerk: item.schuhwerk,
                        type: item.type,
                        flutlicht: item.flutlicht,
                        code: item.code,
                        address: item.address
                    }));
                    setDataSource(formattedData);
                }
            } catch (error) {
                message.error('Failed to fetch stadiums');
                console.error('Fetch stadiums error:', error);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, []);

    /**
     * Sets the stadium detail in session storage.
     *
     * @param {Object} value - The stadium detail to be stored.
     */
    const setStadiumDetail = (value) => {
        console.log("Setting stadium detail in session storage:", value);
        sessionStorage.setItem("stadiumDetail", JSON.stringify(value));
    };

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
        },
        {
            title: 'Bundesland',
            dataIndex: 'bundesland',
        },
        {
            title: 'Partner Since',
            dataIndex: 'partner_since',
        },
        {
            title: 'Home Team',
            dataIndex: 'home_team',
        },
        {
            title: 'Address',
            dataIndex: 'address',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Button
                    type="text"
                    onClick={() => {
                        setStadiumDetail(record);
                        history.push(`/admin/stadium_detail/${record.id}`);
                    }}
                >
                    Stadium Page
                </Button>
            ),
        },
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Stadium Übersicht</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Stadium Übersicht" style={{ marginTop: 16 }}>
                <Table dataSource={dataSource} columns={columns} loading={loading} />
            </Card>
        </div>
    );
}
