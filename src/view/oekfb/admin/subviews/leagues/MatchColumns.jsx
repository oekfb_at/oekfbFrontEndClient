// MatchColumns.jsx

import React from 'react';
import { DatePicker, TimePicker, Select, Button, Tooltip } from 'antd';
import moment from 'moment';

const { Option } = Select;

const MatchColumns = ({
                          teams,
                          updatedMatches,
                          setUpdatedMatches,
                          referees,
                          stadiums,
                          stadiumIdToNameMap,
                          handleUpdateMatch,
                          handleGoMatchDetail,
                      }) => {

    // Helper function to get team by ID
    const getTeamById = (id) => {
        return teams.find(team => team.id === id);
    };

    // Function to handle input changes
    const handleInputChange = (matchId, field, value) => {
        if (field === 'stadium') {
            const stadiumName = stadiumIdToNameMap[value] || 'Unknown';
            setUpdatedMatches(prevState => ({
                ...prevState,
                [matchId]: {
                    ...prevState[matchId],
                    stadium: value,
                    location: stadiumName,
                }
            }));
        }
        else {
            setUpdatedMatches(prevState => ({
                ...prevState,
                [matchId]: {
                    ...prevState[matchId],
                    [field]: value,
                }
            }));
        }
    };

    return [
        {
            title: 'Home',
            dataIndex: ['home_team', 'id'],
            key: 'home_team',
            render: (id) => {
                const team = getTeamById(id);
                if (!team) {
                    return 'Unknown Team';
                }
                return (
                    <a href={`/admin/team_detail/${team.id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                        <div style={{ textAlign: 'center' }}>
                            {team.logo ? (
                                <img
                                    src={team.logo}
                                    alt="Home Team Logo"
                                    style={{ maxWidth: '30px', maxHeight: '30px', marginBottom: '4px' }}
                                />
                            ) : (
                                <div style={{
                                    width: '30px',
                                    height: '30px',
                                    backgroundColor: '#f0f0f0',
                                    display: 'inline-block',
                                    marginBottom: '4px'
                                }} />
                            )}
                            <div>{team.team_name || 'Unknown'}</div>
                        </div>
                    </a>
                );
            },
        },
        {
            title: 'Away',
            dataIndex: ['away_team', 'id'],
            key: 'away_team',
            render: (id) => {
                const team = getTeamById(id);
                if (!team) {
                    return 'Unknown Team';
                }
                return (
                    <a href={`/admin/team_detail/${team.id}`} style={{ textDecoration: 'none', color: 'inherit' }}>
                        <div style={{ textAlign: 'center' }}>
                            {team.logo ? (
                                <img
                                    src={team.logo}
                                    alt="Away Team Logo"
                                    style={{ maxWidth: '30px', maxHeight: '30px', marginBottom: '4px' }}
                                />
                            ) : (
                                <div style={{
                                    width: '30px',
                                    height: '30px',
                                    backgroundColor: '#f0f0f0',
                                    display: 'inline-block',
                                    marginBottom: '4px'
                                }} />
                            )}
                            <div>{team.team_name || 'Unknown'}</div>
                        </div>
                    </a>
                );
            },
        },
        {
            title: 'Score',
            dataIndex: 'score',
            key: 'score',
            render: (score) => {
                if (!score) return '0 - 0';
                const home = score.home !== undefined ? score.home : 0;
                const away = score.away !== undefined ? score.away : 0;
                return `${home} - ${away}`;
            }
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            render: (status) => status ? status.charAt(0).toUpperCase() + status.slice(1) : 'N/A',
            filters: [
                { text: 'Pending', value: 'pending' },
                { text: 'Completed', value: 'completed' },
                // Add other statuses as needed
            ],
            onFilter: (value, record) => record.status === value,
        },
        {
            title: 'Gameday',
            dataIndex: ['details', 'gameday'],
            key: 'gameday',
            render: (gameday) => gameday !== undefined && gameday !== null ? gameday : 'N/A',
            sorter: (a, b) => (a.details?.gameday || 0) - (b.details?.gameday || 0),
        },
        {
            title: 'Date and Time',
            key: 'dateTime',
            render: (text, record) => {
                const existingDateTime = record.details?.date ? moment.parseZone(record.details.date) : null;
                const updatedDateTime = updatedMatches[record.id]?.dateTime
                    ? moment.parseZone(updatedMatches[record.id].dateTime)
                    : existingDateTime;

                const currentDate = updatedDateTime ? updatedDateTime.clone().startOf('day') : null;
                const currentTime = updatedDateTime ? updatedDateTime.clone().set({ second: 0, millisecond: 0 }) : null;

                return (
                    <div style={{ display: 'flex', gap: '8px' }}>
                        {/* Date Selector */}
                        <DatePicker
                            value={currentDate}
                            onChange={(date) => {
                                const newDateTime = date
                                    ? date.clone().set({
                                        hour: updatedDateTime?.hour() || 0,
                                        minute: updatedDateTime?.minute() || 0,
                                        second: 0,
                                        millisecond: 0,
                                    })
                                    : null;
                                handleInputChange(record.id, 'dateTime', newDateTime ? newDateTime.utc().format() : null);
                            }}
                            format="YYYY-MM-DD"
                            style={{ width: '100%', minWidth: '150px' }}
                        />
                        {/* Time Selector */}
                        <TimePicker
                            value={currentTime}
                            onChange={(time) => {
                                const newDateTime = time
                                    ? (updatedDateTime || moment())
                                        .clone()
                                        .set({ hour: time.hour(), minute: 0, second: 0, millisecond: 0 })
                                    : null;
                                handleInputChange(record.id, 'dateTime', newDateTime ? newDateTime.utc().format() : null);
                            }}
                            format="HH:mm"
                            showNow={false}
                            style={{ width: '100%', minWidth: '100px' }}
                        />
                    </div>
                );
            },
        }
        ,
        {
            title: 'Referee',
            key: 'referee',
            render: (text, record) => {
                const selectedReferee = updatedMatches[record.id]?.referee !== undefined
                    ? updatedMatches[record.id].referee
                    : record.referee?.id || '';

                return (
                    <Select
                        style={{ width: '100%', minWidth: '150px' }}  // Add minWidth here
                        placeholder="Select Referee"
                        value={selectedReferee}
                        onChange={(value) => handleInputChange(record.id, 'referee', value)}
                        allowClear
                    >
                        {referees.map(referee => (
                            referee.id && (
                                <Option key={referee.id} value={referee.id}>
                                    {referee.name || 'Unnamed Referee'}
                                </Option>
                            )
                        ))}
                    </Select>
                );
            },
        },
        {
            title: 'Stadium',
            key: 'stadium',
            render: (text, record) => {
                const selectedStadium = updatedMatches[record.id]?.stadium !== undefined
                    ? updatedMatches[record.id].stadium
                    : record.details?.stadium || '';

                return (
                    <Select
                        style={{ width: "100%" }}
                        placeholder="Select Stadium"
                        value={selectedStadium}
                        onChange={(value) => handleInputChange(record.id, 'stadium', value)}
                        allowClear
                    >
                        {stadiums.map(stadium => (
                            stadium.id && (
                                <Option key={stadium.id} value={stadium.id}>
                                    {stadium.name || 'Unnamed Stadium'}
                                </Option>
                            )
                        ))}
                    </Select>
                );
            },
        },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button
                    type="primary"
                    onClick={() => handleUpdateMatch(record.id)}
                    disabled={!updatedMatches[record.id]}
                >
                    Update
                </Button>
            ),
        },
        {
            title: 'Detail',
            key: 'details',
            render: (text, record) => (
                <Button
                    type="default"
                    onClick={() => handleGoMatchDetail(record.id)}
                >
                    Details
                </Button>
            ),
        },
    ];
};

export default MatchColumns;
