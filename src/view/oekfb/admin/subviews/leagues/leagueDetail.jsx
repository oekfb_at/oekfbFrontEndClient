import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Breadcrumb, Card, Descriptions, Spin, Input, Button, Table, message, Modal } from 'antd';
import ApiService from "../../../../../network/ApiService";
import { useHistory } from 'react-router-dom';
import FirebaseImageUpload from "../../../../../network/Firebase/storage/FirebaseImageUpload";

export default function LeagueDetail() {
    const { id } = useParams();
    const history = useHistory();

    const [league, setLeague] = useState(null);
    const [originalLeague, setOriginalLeague] = useState(null);
    const [loading, setLoading] = useState(true);
    const [seasons, setSeasons] = useState([]);
    const [teams, setTeams] = useState([]);
    const [rounds, setRounds] = useState(2); // Default number of rounds

    useEffect(() => {
        const fetchLeague = async () => {
            setLoading(true);
            try {
                const storedLeague = JSON.parse(sessionStorage.getItem("selectedLeague"));
                if (storedLeague) {
                    setLeague(storedLeague);
                    setOriginalLeague(storedLeague);
                    const response = await new ApiService().get(`leagues/${storedLeague.id}/seasons`);
                    setSeasons(response.league.seasons);
                    setTeams(response.league.teams);
                } else {
                    const response = await new ApiService().get(`leagues/${id}`);
                    setLeague(response);
                    setOriginalLeague(response);
                    const seasonResponse = await new ApiService().get(`leagues/${response.id}/seasons`);
                    setSeasons(seasonResponse.league.seasons);
                    setTeams(seasonResponse.league.teams);
                }
            } catch (error) {
                console.error('Failed to fetch league details:', error);
            } finally {
                setLoading(false);
            }
        };
        fetchLeague();
    }, [id]);

    const handleInputChange = (field, value) => {
        const keys = field.split('.');
        let updatedData = { ...league };

        let temp = updatedData;
        for (let i = 0; i < keys.length - 1; i++) {
            if (!temp[keys[i]]) {
                temp[keys[i]] = {}; // Initialize missing nested objects
            }
            temp = temp[keys[i]];
        }

        // Parse numeric values for specific fields
        temp[keys[keys.length - 1]] = field === 'hourly' ? parseFloat(value) : value;

        setLeague(updatedData); // Update the league state
    };

    const deepEqual = (obj1, obj2) => {
        if (obj1 === obj2) return true;
        if (typeof obj1 !== typeof obj2) return false;
        if (typeof obj1 !== 'object' || obj1 === null || obj2 === null) return false;

        const keys1 = Object.keys(obj1);
        const keys2 = Object.keys(obj2);
        if (keys1.length !== keys2.length) return false;

        for (const key of keys1) {
            if (!keys2.includes(key) || !deepEqual(obj1[key], obj2[key])) {
                return false;
            }
        }
        return true;
    };

    const hasLeagueChanged = () => {
        return JSON.stringify(league) !== JSON.stringify(originalLeague);
    };

    const handleUpdateLeague = async () => {
        if (!league.name) {
            message.error("Der Liga Name darf nicht leer sein.");
            return;
        }

        try {
            const cleanedLeague = Object.fromEntries(
                Object.entries(league).map(([key, value]) => [key, value ?? ""])
            );

            console.log('Updated league payload:', cleanedLeague); // Debug log

            await new ApiService().patch(`leagues/${league.id}`, cleanedLeague);
            setOriginalLeague(league); // Update the original data after successful save
            sessionStorage.setItem("selectedLeague", JSON.stringify(league));
            message.success("League updated successfully");
        } catch (error) {
            message.error("Failed to update league");
            console.error("Update league error:", error);
        }
    };

    const handleCreateSeason = async () => {
        const apiService = new ApiService();
        try {
            await apiService.post(`leagues/${league.id}/createSeason/${rounds}`);
            message.success('Season created successfully');
            setLoading(true);
            setTimeout(async () => {
                const response = await apiService.get(`leagues/${league.id}/seasons`);
                setSeasons(response.league.seasons);
                setLoading(false);
            }, 3000);
        } catch (error) {
            message.error('Failed to create season');
            console.error('Create season error:', error);
        }
    };

    const handleUpload = async (url, field) => {
        try {
            const updatedLeague = {
                ...league,
                homepagedata: {
                    ...league.homepagedata,
                    sliderdata: [
                        {
                            ...league.homepagedata?.sliderdata?.[0],
                            [field]: url,
                        },
                    ],
                },
            };

            await new ApiService().patch(`leagues/${league.id}`, updatedLeague);
            setLeague(updatedLeague);
            setOriginalLeague(updatedLeague);
            message.success("Hero image uploaded and league updated successfully");
        } catch (error) {
            console.error(`Error updating league with uploaded file: ${field}`, error);
            message.error(`Failed to update league with the uploaded file: ${field}`);
        }
    };

    const handleDeleteSeason = async (seasonId) => {
        const apiService = new ApiService();
        try {
            await apiService.delete(`seasons/matches/${seasonId}`);
            message.success('Season deleted successfully');
            const response = await apiService.get(`leagues/${league.id}/seasons`);
            setSeasons(response.league.seasons);
        } catch (error) {
            message.error('Failed to delete season');
            console.error('Delete season error:', error);
        }
    };

    const setSeasonDetail = (value) => {
        sessionStorage.setItem("seasonDetail", JSON.stringify(value));
        sessionStorage.setItem("seasonDetailTeams", JSON.stringify(teams));
    };

    const confirmDelete = (seasonId) => {
        Modal.confirm({
            title: 'Are you sure you want to delete this season with all matches?',
            content: 'This action cannot be undone.',
            onOk: () => handleDeleteSeason(seasonId),
            okText: 'Yes',
            cancelText: 'No',
            okButtonProps: { danger: true },
        });
    };

    const seasonColumns = [
        { title: 'Season Name', dataIndex: 'name', key: 'name' },
        { title: 'Start Date', dataIndex: 'start_date', key: 'start_date' },
        {
            title: 'End Date',
            key: 'end_date',
            render: (text, record) => (
                <Button type="primary" danger onClick={() => confirmDelete(record.id)}>
                    Saison Löschen
                </Button>
            ),
        },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button type="link" onClick={() => {
                    setSeasonDetail(record);
                    history.push(`/admin/season_detail/${record.id}`);
                }}
                >
                    Zum Spielplan
                </Button>
            ),
        }
    ];



    const teamColumns = [
        {
            title: 'Logo',
            dataIndex: 'logo',
            key: 'logo',
            render: (logo) => logo ? <img src={`${logo}`} alt="logo" style={{ maxWidth: '30px', maxHeight: '30px' }} /> : 'N/A'
        },
        { title: 'Mannschaft', dataIndex: 'team_name', key: 'team_name' },
        { title: 'Liga', dataIndex: ['league_code'], key: 'league_code', render: (league_code) => league_code || 'N/A' },
        { title: 'Durschn. Alter', dataIndex: 'average_age', key: 'average_age' },
        { title: 'Mitglied seit', dataIndex: 'membership_since', key: 'membership_since' },
        {
            title: 'Actions',
            key: 'actions',
            render: (text, record) => (
                <Button type="text" onClick={() => setSeasonDetail(record)}>
                    Spieler Seite
                </Button>
            ),
        }
    ];

    if (loading) {
        return <Spin size="large" />;
    }

    if (!league) {
        return <p>League not found</p>;
    }

    return (
        <div style={{ padding: 24 }}>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/admin/league">Ligen</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Liga Detail</Breadcrumb.Item>
                </Breadcrumb>

                {/*<Button type="primary" onClick={handleUpdateLeague} disabled={!hasLeagueChanged()}>*/}
                    <Button type="primary" onClick={handleUpdateLeague}>
                    Aktualisieren
                </Button>
            </div>

            <Card title="Liga Information" style={{ marginTop: 16 }}>
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Liga Name">
                        <Input value={league.name} onChange={(e) => handleInputChange('name', e.target.value)} />
                    </Descriptions.Item>
                    <Descriptions.Item label="Liga Code">
                        <Input value={league.code} onChange={(e) => handleInputChange('code', e.target.value)} />
                    </Descriptions.Item>
                    <Descriptions.Item label="Bundesland">{league.state}</Descriptions.Item>
                    <Descriptions.Item label="Schiedrichter Std Rate (€)">
                        <Input
                            type="number"
                            value={league.hourly}
                            onChange={(e) => handleInputChange('hourly', e.target.value)}
                        />
                    </Descriptions.Item>
                    <Descriptions.Item label="Maximale Team Anzahl">
                        <Input
                            type="number"
                            value={league.teamcount}
                            onChange={(e) => handleInputChange('teamcount', parseInt(e.target.value, 10))}
                        />
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Homepage" style={{ marginTop: 16 }}>
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Wochenbericht">
                        <Input value={league.homepagedata?.wochenbericht || ''} onChange={(e) => handleInputChange('homepagedata.wochenbericht', e.target.value)} />
                    </Descriptions.Item>
                    <Descriptions.Item label="YouTube Link">
                        <Input value={league?.youtube ?? ''} placeholder="YouTube link Eintrgen" onChange={(e) => handleInputChange('youtube', e.target.value)} />
                    </Descriptions.Item>
                    <Descriptions.Item label="Slider Title">
                        <Input value={league.homepagedata?.sliderdata?.[0]?.title || ''} onChange={(e) => handleInputChange('homepagedata.sliderdata.0.title', e.target.value)} />
                    </Descriptions.Item>
                    <Descriptions.Item label="Slider Description">
                        <Input value={league.homepagedata?.sliderdata?.[0]?.description || ''} onChange={(e) => handleInputChange('homepagedata.sliderdata.0.description', e.target.value)} />
                    </Descriptions.Item>
                    <Descriptions.Item label="Foto">
                        <FirebaseImageUpload
                            existingUrl={league?.homepagedata?.sliderdata?.[0]?.image}
                            path={`league/${league.name}/homepage/hero`}
                            filename={`hero`}
                            buttonText={"Foto Hochladen"}
                            onUploadSuccess={(url) => handleUpload(url, 'image')}
                        />
                    </Descriptions.Item>
                </Descriptions>
            </Card>

            <Card title="Saisons" style={{ marginTop: 16 }}>
                <Table columns={seasonColumns} dataSource={seasons} pagination={false} rowKey="id" />
            </Card>

            <Card title={`Teams (${teams.length})`} style={{ marginTop: 16 }}>
                <Table columns={teamColumns} dataSource={teams} pagination={false} rowKey="id" />
            </Card>
        </div>
    );
}


