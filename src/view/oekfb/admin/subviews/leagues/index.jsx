import React, { useEffect, useState } from 'react';
import { Breadcrumb, Card, Table, Button, message } from 'antd';
import { useHistory } from 'react-router-dom';
import ApiService from '../../../../../network/ApiService';

export default function LeagueIndex() {
    const [leagues, setLeagues] = useState([]);
    const [loading, setLoading] = useState(true);
    const history = useHistory();

    useEffect(() => {
        const fetchLeagues = async () => {
            const apiService = new ApiService();
            try {
                const data = await apiService.get('leagues?per=100');
                const sortedLeagues = data.items.sort((a, b) => a.name.localeCompare(b.name));
                setLeagues(sortedLeagues);
                setLoading(false);
            } catch (error) {
                message.error('Failed to fetch leagues');
                setLoading(false);
            }
        };

        fetchLeagues();
    }, []);

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Code',
            dataIndex: 'code',
            key: 'code',
        },
        {
            title: 'Bundesland',
            dataIndex: 'state',
            key: 'state',
        },
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
                <Button type="text" href={`/admin/league_detail/${record.id}`}>
                    Details
                </Button>
            ),
        },
    ];

    return (
        <div style={{ padding: 24 }}>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/admin/">Administration</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>Ligen Übersicht</Breadcrumb.Item>
            </Breadcrumb>

            <Card title="Ligen Übersicht" style={{ marginTop: 16 }}>
                <Table
                    columns={columns}
                    dataSource={leagues}
                    loading={loading}
                    rowKey="id"
                    pagination={{ pageSize: 100 }}
                />
            </Card>
        </div>
    );
}
