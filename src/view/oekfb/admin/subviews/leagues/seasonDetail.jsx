// SeasonDetail.jsx
import React, { useState, useEffect } from 'react';
import {
    Breadcrumb,
    Card,
    Descriptions,
    Spin,
    message,
    Table, Button, Drawer, Form, Row, Col, InputNumber, Input,
} from 'antd';
import ApiService from "../../../../../network/ApiService";
import { useHistory, useParams } from 'react-router-dom';
import MatchColumns from './MatchColumns';
import moment from 'moment';
import {RiCloseFill} from "react-icons/ri";
import TeamRechnungModal from "../../../../pages/invoice/TeamRechnungModal";

export default function SeasonDetail() {
    const history = useHistory();
    const { id } = useParams(); // Ensure your route parameter is named 'id'
    const [season, setSeason] = useState(null);
    const [home, setHome] = useState(null);
    const [away, setAway] = useState(null);
    const [visible, setVisible] = useState(false);
    const [loading, setLoading] = useState(true);
    const [gameDay, setGameDay] = useState("0");
    const [homeTeamModalVisible, setHomeTeamModalVisible] = useState(false);
    const [awayTeamModalVisible, setAwayTeamModalVisible] = useState(false);
    const [matchesByGameday, setMatchesByGameday] = useState({});
    const [referees, setReferees] = useState([]);
    const [teams, setTeams] = useState([]);
    const [updatedMatches, setUpdatedMatches] = useState({});
    const [stadiums, setStadiums] = useState([]);
    const [stadiumIdToNameMap, setStadiumIdToNameMap] = useState({});

    useEffect(() => {
        const fetchSeasonAndTeams = async () => {
            setLoading(true);
            try {
                let storedSeason = JSON.parse(sessionStorage.getItem("seasonDetail"));
                let storedTeams = JSON.parse(sessionStorage.getItem("seasonDetailTeams"));

                if (!storedSeason) {
                    // Fetch the season data from the API
                    const seasonResponse = await new ApiService().get(`seasons/${id}`);
                    if (seasonResponse && seasonResponse.id) {
                        storedSeason = seasonResponse;
                        sessionStorage.setItem("seasonDetail", JSON.stringify(storedSeason));
                    } else {
                        throw new Error('Invalid season data received');
                    }
                }

                if (!storedTeams) {
                    // Fetch the teams data from the API
                    const teamsResponse = await new ApiService().get(`seasons/${storedSeason.id}/teams`);
                    if (teamsResponse && teamsResponse.teams) {
                        storedTeams = teamsResponse.teams;
                        sessionStorage.setItem("seasonDetailTeams", JSON.stringify(storedTeams));
                    } else {
                        throw new Error('Invalid teams data received');
                    }
                }

                setSeason(storedSeason);
                setTeams(storedTeams);

                // Fetch matches
                const matchesResponse = await new ApiService().get(`seasons/${storedSeason.id}/matches`);
                if (matchesResponse && matchesResponse.matches) {
                    setMatchesByGameday(groupMatchesByGameday(matchesResponse.matches));
                } else {
                    throw new Error('Invalid matches data received');
                }

                // Fetch stadiums
                const stadiumsResponse = await new ApiService().get('stadiums');
                if (stadiumsResponse && stadiumsResponse.items) {
                    setStadiums(stadiumsResponse.items);
                    const stadiumMap = stadiumsResponse.items.reduce((acc, stadium) => {
                        acc[stadium.id] = stadium.name;
                        return acc;
                    }, {});
                    setStadiumIdToNameMap(stadiumMap);
                } else {
                    throw new Error('Invalid stadiums data received');
                }

                // Fetch referees
                const refereesResponse = await new ApiService().get('referees?per=1000');
                if (refereesResponse && refereesResponse.items) {
                    setReferees(refereesResponse.items);
                } else {
                    throw new Error('Invalid referees data received');
                }

            } catch (error) {
                console.error('Failed to fetch season details:', error);
                message.error('Failed to load season details. Please try again later.');
            } finally {
                setLoading(false);
            }
        };
        fetchSeasonAndTeams();
    }, [id]);

    const konfig = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    const onHomeSelectTeam = () => {
        setHomeTeamModalVisible(true);
    };

    const handleHomeTeamSelect = (team) => {
        console.log("Selected team:", team);
        setHome(team);
        setHomeTeamModalVisible(false);
    };

    const handleAwayTeamSelect = (team) => {
        console.log("Selected team:", team);
        setAway(team);
        setAwayTeamModalVisible(false);
    };

    const onAwaySelectTeam = () => {
        setAwayTeamModalVisible(true);
    };

    const groupMatchesByGameday = (matches) => {
        return matches.reduce((acc, match) => {
            const gameday = match.details?.gameday;
            if (gameday === undefined || gameday === null) {
                // Handle matches without a gameday
                if (!acc['Unknown']) {
                    acc['Unknown'] = [];
                }
                acc['Unknown'].push(match);
            } else {
                if (!acc[gameday]) {
                    acc[gameday] = [];
                }
                acc[gameday].push(match);
            }
            return acc;
        }, {});
    };

    const handleGoMatchDetail = (matchId) => {
        history.push(`/admin/match_detail/${matchId}`);
    };

    const handleUpdateMatch = async (matchId) => {
        const matchUpdates = updatedMatches[matchId] || {};
        try {
            const existingMatch = Object.values(matchesByGameday).flat().find(match => match.id === matchId);

            if (!existingMatch) {
                message.error('Match not found');
                return;
            }

            const dateTime = matchUpdates.dateTime !== undefined ? matchUpdates.dateTime : existingMatch.details?.date;

            const requestBody = {
                id: matchId,
                bericht: existingMatch.bericht !== undefined ? existingMatch.bericht : null,
                season: { id: existingMatch.season?.id || null },
                home_team: { id: existingMatch.home_team?.id || null },
                away_team: { id: existingMatch.away_team?.id || null },
                referee: { id: matchUpdates.referee !== undefined ? matchUpdates.referee : existingMatch.referee?.id || null },
                status: existingMatch.status,
                first_half_start_date: existingMatch.first_half_start_date || null,
                second_half_start_date: existingMatch.second_half_start_date || null,
                score: {
                    home: existingMatch.score?.home || 0,
                    away: existingMatch.score?.away || 0,
                },
                details: {
                    gameday: existingMatch.details?.gameday || null,
                    date: matchUpdates.dateTime !== undefined ? matchUpdates.dateTime : existingMatch.details?.date,
                    stadium: matchUpdates.stadium !== undefined ? matchUpdates.stadium : existingMatch.details?.stadium || null,
                    location: matchUpdates.location !== undefined ? matchUpdates.location : existingMatch.details?.location || null,
                },
                homeBlanket: existingMatch.homeBlanket,
                awayBlanket: existingMatch.awayBlanket
            };


            // Validate requestBody before sending
            if (!requestBody.season.id || !requestBody.home_team.id || !requestBody.away_team.id) {
                throw new Error('Missing essential match information');
            }

            await new ApiService().patch(`matches/${matchId}`, requestBody);
            message.success('Match updated successfully');

            // Update local state to reflect changes
            setMatchesByGameday(prevState => {
                const newState = { ...prevState };
                Object.keys(newState).forEach(gameday => {
                    newState[gameday] = newState[gameday].map(match =>
                        match.id === matchId ? { ...match, ...requestBody } : match
                    );
                });
                return newState;
            });

            // Clear updatedMatches for this match
            setUpdatedMatches(prevState => {
                const newState = { ...prevState };
                delete newState[matchId];
                return newState;
            });

        } catch (error) {
            message.error('Failed to update match');
            console.error('Update match error:', error);
        }
    };

    const reqData = {
        homeTeamId: home?.id,
        awayTeamId: away?.id,
        gameday: gameDay,
        seasonId: season?.id,
    };

    const handleErstellen = async () => {
        if (!reqData) {
            message.error("Please preview the invoice before creating it.");
            return;
        }

        try {
            console.log(reqData);
            // POST request using ApiService
            await new ApiService().post('/matches/create/internal', reqData);
            console.log('Match created successfully!');
            // Refresh the page
            window.location.reload();
        } catch (error) {
            console.error("Error creating invoice:", error);
        }
    };

    if (loading) {
        return <Spin size="large" />;
    }

    if (!season) {
        return <p>Saison nicht gefunden</p>;
    }

    const totalMatches = Object.values(matchesByGameday).flat().length;

    return (
        <div style={{ padding: 24, maxWidth: "none" }}>

            <div style={{display: "flex", flexDirection: "row", width:"100%", justifyContent: "space-between"}}>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/admin/">Administration</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href={`/admin/season_detail/${season?.id || ''}`}>Liga</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>Saison Detail</Breadcrumb.Item>
                </Breadcrumb>
                <Button style={{ marginBottom: "20px", width: "200px"}} onClick={konfig} className="hp-mb-16" type="primary" block >
                    Neues Spiel Anlegen
                </Button>
            </div>

            <Card title="Saison Informationen" style={{ marginTop: 16 }}>
                <Descriptions bordered column={1}>
                    <Descriptions.Item label="Saison Name">{season.name || 'N/A'}</Descriptions.Item>
                    <Descriptions.Item label="Liga ID">{season.league?.id || 'N/A'}</Descriptions.Item>
                    <Descriptions.Item label="Details">{season.details || 'N/A'}</Descriptions.Item>
                    <Descriptions.Item label="Anzahl Spieltage">{Object.keys(matchesByGameday).length}</Descriptions.Item>
                    <Descriptions.Item label="Anzahl Spiele">{totalMatches}</Descriptions.Item>
                </Descriptions>
            </Card>

            {Object.keys(matchesByGameday).map(gameday => (
                <Card
                    key={gameday}
                    title={`Spieltag ${gameday} (Spiele: ${matchesByGameday[gameday].length})`}
                    style={{ marginTop: 16 }}
                >
                    <Table
                        columns={MatchColumns({
                            teams,
                            updatedMatches,
                            setUpdatedMatches,
                            referees,
                            stadiums,
                            stadiumIdToNameMap,
                            handleUpdateMatch,
                            handleGoMatchDetail,
                        })}
                        dataSource={matchesByGameday[gameday].map(match => ({
                            ...match,
                            key: match.id, // Ant Design Table requires a unique 'key' prop
                        }))}
                        pagination={false}
                        rowKey="id"
                    />
                </Card>
            ))}

            <Drawer
                className="hp-drawer-submit"
                title="Send Invoice"
                placement="right"
                closable={false}
                onClose={onClose}
                visible={visible}
                width={400}
                bodyStyle={{ paddingBottom: 80 }}
                closeIcon={<RiCloseFill className="remix-icon hp-text-color-black-80" size={24} />}
            >
                <Form layout="vertical" hideRequiredMark>
                    <Row gutter={[16, 16]}>
                        <Col span={24}>
                            <Form.Item label="Heim Mannschaft auswählen" rules={[{ required: true, message: "Bitte wählen Sie ein Team" }]}>
                                {home ? (
                                    <p>{home.team_name}</p>
                                ) : (
                                    <Button type="primary" onClick={onHomeSelectTeam}>
                                        Team auswählen
                                    </Button>
                                )}
                            </Form.Item>
                        </Col>

                        <Col span={24}>
                            <Form.Item label="Auswärts Mannschaft auswählen" rules={[{ required: true, message: "Bitte wählen Sie ein Team" }]}>
                                {away ? (
                                    // <img src={away.image} alt="">
                                    <p>{away.team_name}</p>
                                ) : (
                                    <Button type="primary" onClick={onAwaySelectTeam}>
                                        Team auswählen
                                    </Button>
                                )}
                            </Form.Item>
                        </Col>

                        <Col span={24}>
                            <Form.Item
                                name="gameDay"
                                label="Spieltag"
                                rules={[{ required: true, message: "Bitte Spieltag eingeben" }]}
                            >
                                <InputNumber
                                    placeholder="Spieltag"
                                    value={gameDay}
                                    onChange={(value) => setGameDay(value)}
                                    // formatter={(value) => `€ ${value}`}
                                    // parser={(value) => value.replace('€', '').trim()}
                                    style={{ width: "100%" }}
                                />
                            </Form.Item>
                        </Col>

                        <Col span={24}>
                            <Button
                                onClick={handleErstellen}
                                type="primary"
                                className="hp-mr-8"
                            >
                                Create
                            </Button>

                            <Button onClick={onClose} type="text">
                                Cancel
                            </Button>
                        </Col>
                    </Row>
                </Form>
            </Drawer>

            <TeamRechnungModal
                visible={homeTeamModalVisible}
                onClose={() => setHomeTeamModalVisible(false)}
                onTeamSelect={handleHomeTeamSelect}
            />

            <TeamRechnungModal
                visible={awayTeamModalVisible}
                onClose={() => setAwayTeamModalVisible(false)}
                onTeamSelect={handleAwayTeamSelect}
            />


        </div>
    );
}
