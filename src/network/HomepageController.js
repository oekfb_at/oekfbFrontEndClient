import ApiService from './ApiService';

/**
 * HomepageController class to interact with the Homepage API endpoints.
 */
class HomepageController {
    constructor() {
        this.apiService = new ApiService();
    }

    /**
     * Fetch the list of leagues.
     * GET /leagueList
     * @returns {Promise<Object[]>} Array of league objects.
     */
    async fetchLeagueList() {
        return this.apiService.get('leagueList');
    }

    /**
     * Fetch the homepage data for a specific league.
     * GET /:id/homepage
     * @param {string} leagueId - The UUID of the league.
     * @returns {Promise<Object>} Homepage data for the league.
     */
    async fetchHomepage(leagueId) {
        return this.apiService.get(`${leagueId}/homepage`);
    }

    /**
     * Fetch live scores for a specific league.
     * GET /:id/homepage/livescore
     * @param {string} leagueId - The UUID of the league.
     * @returns {Promise<Object[]>} Array of live match objects.
     */
    async fetchLivescore(leagueId) {
        return this.apiService.get(`${leagueId}/homepage/livescore`);
    }

    /**
     * Fetch the clubs of a specific league.
     * GET /:id/homepage/clubs
     * @param {string} leagueId - The UUID of the league.
     * @returns {Promise<Object[]>} Array of club objects.
     */
    async fetchLeagueClubs(leagueId) {
        return this.apiService.get(`${leagueId}/homepage/clubs`);
    }

    /**
     * Fetch details of a specific club.
     * GET /homepage/clubs/:id
     * @param {string} clubId - The UUID of the club.
     * @returns {Promise<Object>} Club details.
     */
    async fetchClub(clubId) {
        return this.apiService.get(`homepage/clubs/${clubId}`);
    }

    /**
     * Fetch details of a specific player.
     * GET /homepage/clubs/players/:playerID
     * @param {string} playerId - The UUID of the player.
     * @returns {Promise<Object>} Player details.
     */
    async fetchPlayerDetail(playerId) {
        return this.apiService.get(`homepage/clubs/players/${playerId}`);
    }

    /**
     * Fetch news for a specific league.
     * GET /:id/homepage/news
     * @param {string} leagueId - The UUID of the league.
     * @returns {Promise<Object[]>} Array of news items.
     */
    async fetchLeagueNews(leagueId) {
        return this.apiService.get(`${leagueId}/homepage/news`);
    }

    /**
     * Fetch details of a specific news item.
     * GET /homepage/news/:newsItemID
     * @param {string} newsItemId - The UUID of the news item.
     * @returns {Promise<Object>} News item details.
     */
    async fetchNewsItem(newsItemId) {
        return this.apiService.get(`homepage/news/${newsItemId}`);
    }

    /**
     * Register a new team.
     * POST /homepage/register
     * @param {Object} registrationData - Registration data.
     * @param {Object} registrationData.primaryContact - Primary contact information.
     * @param {Object} registrationData.secondaryContact - Secondary contact information.
     * @param {string} registrationData.teamName - Name of the team.
     * @param {string} registrationData.verein - Verein information.
     * @param {string} registrationData.referCode - Referral code.
     * @param {string} registrationData.bundesland - Bundesland information.
     * @param {string} [registrationData.initialPassword] - Initial password (optional).
     * @returns {Promise<Object>} Response status.
     */
    async register(registrationData) {
        return this.apiService.post(`homepage/register`, registrationData);
    }

    /**
     * Fetch the goal leaderboard for a specific league.
     * GET /:id/goalLeaderBoard
     * @param {string} leagueId - The UUID of the league.
     * @returns {Promise<Object[]>} Array of leaderboard objects for goals.
     * @example
     * const leaderBoard = await homepageController.fetchGoalLeaderBoard('league-id');
     */
    async fetchGoalLeaderBoard(leagueId) {
        return this.apiService.get(`${leagueId}/goalLeaderBoard`);
    }

    /**
     * Fetch the red card leaderboard for a specific league.
     * GET /:id/redCardLeaderBoard
     * @param {string} leagueId - The UUID of the league.
     * @returns {Promise<Object[]>} Array of leaderboard objects for red cards.
     * @example
     * const leaderBoard = await homepageController.fetchRedCardLeaderBoard('league-id');
     */
    async fetchRedCardLeaderBoard(leagueId) {
        return this.apiService.get(`${leagueId}/redCardLeaderBoard`);
    }

    /**
     * Fetch the yellow card leaderboard for a specific league.
     * GET /:id/yellowCardLeaderBoard
     * @param {string} leagueId - The UUID of the league.
     * @returns {Promise<Object[]>} Array of leaderboard objects for yellow cards.
     * @example
     * const leaderBoard = await homepageController.fetchYellowCardLeaderBoard('league-id');
     */
    async fetchYellowCardLeaderBoard(leagueId) {
        return this.apiService.get(`${leagueId}/yellowCardLeaderBoard`);
    }

}

export default HomepageController;
