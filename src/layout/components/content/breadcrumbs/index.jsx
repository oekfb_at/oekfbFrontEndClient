import { Link } from "react-router-dom";
import { Col, Breadcrumb } from "antd";

const BreadCrumbs = (props) => {
  const {
    breadCrumbParent,
    breadCrumbParent2,
    breadCrumbParent3,
    breadCrumbActive,
  } = props;

  return (
    <Col>
      <Breadcrumb className="hp-d-flex hp-flex-wrap">
        <Breadcrumb.Item>
          <Link to="/admin">Administration</Link>
        </Breadcrumb.Item>

        <Breadcrumb.Item>
          <Link to="/admin/rechnungen">Rechnungen Index</Link>
        </Breadcrumb.Item>

        <Breadcrumb.Item>{breadCrumbActive}</Breadcrumb.Item>
      </Breadcrumb>
    </Col>
  );
};

export default BreadCrumbs;